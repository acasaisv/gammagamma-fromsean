from __future__ import print_function
#from ROOT import TH1F, TCanvas, gStyle, TLegend, TGraph,kRed,kBlue,kGreen
from ROOT import *
from sklearn.externals import joblib
from array import array
import pickle
from scipy.stats import ks_2samp
import numpy as np
import datetime
import math


from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler

gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")
trainFraction = 0.5
cType = 'OnlineTupleNone'
req_indices = {
    'OnlineTupleLL': [8, 5, 181, 170, 10, 214],
    'OnlineTupleDD': [5, 181, 170, 10, 214],
    'OnlineTupleNone': [5, 137, 138, 139, 140]
}

bins = 7
gStyle.SetOptTitle(0)
lowbound=0.
histSigTrain = []
histSigTest = []
histBgTrain = []
histBgTest = []
for label in req_indices.keys():
    histSigTrain.append(TH1F("sig_train_{}".format(label), "Signal probability Probability Events", bins, lowbound, 1 + 1e-5)
    histSigTest.append(TH1F("sig_test_{}".format(label), "Test signal Probability Events", bins, lowbound, 1 + 1e-5))
    histBgTrain.append(TH1F("bg_train_{}".format(label), "Training background Probability Events", bins, lowbound, 1 + 1e-5))
    histBgTest.append(TH1F("bg_test_{}".format(label), "Test background Probability Events", bins, lowbound, 1 + 1e-5))

classifier = joblib.load('classifier_{}.pkl'.format(cType))

inputs = ['sig_alps_{}_plot.p'.format(cType), 'sig_combined_{}_plot.p'.format(cType), 'bkg_2018_l0_{}_plot.p'.format(cType)]

print ('Loading total signal data file...')
sig_data_tot = []
for n in range(len(inputs)-1):
    sig_data_tot += joblib.load(inputs[n])
# sig_data_tot = joblib.load(inputs[1])
print ('Loading background data file...')
bkg_data_tot = joblib.load(inputs[-1])

print ('Selecting required information')
sig_data = []
for d in sig_data_tot:
    sig_data.append(np.take(d, req_indices[cType]))
bkg_data = []
for d in bkg_data_tot:
    bkg_data.append(np.take(d, req_indices[cType]))

np.random.shuffle(sig_data)
np.random.shuffle(bkg_data)

print ('Signal size: %s' % len(sig_data))
print ('Background size: %s' % len(bkg_data))
print ('Number of features (sig): %s' % len(sig_data[0]))
print ('Number of features (bkg): %s' % len(bkg_data[0]))
#
cutIndex = int(trainFraction * len(sig_data))
cutIndexBKG = int(trainFraction * len(bkg_data))
#
sigTrain = sig_data[: cutIndex]
sigTest = sig_data[cutIndex:]
#
bgTrain = bkg_data[: cutIndexBKG]
print (cutIndex)
bgTest = bkg_data[cutIndexBKG:]
print (len(bgTest))

# Create the scaler to preprocess the data
scaler = joblib.load('scaler_{}.pkl'.format(cType))

# transform the training sameple
sigTrain = scaler.transform(sigTrain)
# do the same to the test data
sigTest = scaler.transform(sigTest)
# do the same to the test data
bgTrain = scaler.transform(bgTrain)
# do the same to the test data
bgTest = scaler.transform(bgTest)

print (datetime.datetime.now(), 'Learning...')
train = np.append(sigTrain, bgTrain, axis=0)

target = [1] * len(sigTrain) + [-1] * len(bgTrain)
classifier.fit(train, target)


trainingSample = []
for entry in sigTrain:
    probability = float(classifier.predict_proba([entry])[0][1])
    trainingSample.append(probability)
    histSigTrain.Fill(probability)

testSample = []
for entry in sigTest:
    probability = float(classifier.predict_proba([entry])[0][1])
    testSample.append(probability)
    histSigTest.Fill(probability)

print ("Signal", ks_2samp(trainingSample, testSample))

trainingSampleBKG = []
for entry in bgTrain:
    probability = float(classifier.predict_proba([entry])[0][1])
    trainingSampleBKG.append(probability)
    histBgTrain.Fill(probability)

testSampleBKG = []
for entry in bgTest:
    probability = float(classifier.predict_proba([entry])[0][1])
    testSampleBKG.append(probability)
    histBgTest.Fill(probability)

print ("Background", ks_2samp(trainingSample, testSample))

canvas1 = TCanvas('c1', 'Signal probability', 200, 10, 700, 500)
gStyle.SetOptStat(0)

SigTrainColor=kRed
SigTestColor=kGreen+3
histSigTrain.SetLineColor(SigTrainColor)
histSigTest.SetLineColor(SigTestColor)
histSigTrain.SetMarkerColor(SigTrainColor)
histSigTest.SetMarkerColor(SigTestColor)
histSigTrain.SetMarkerStyle(3)
histSigTest.SetMarkerStyle(3)
#
BgTrainColor=kMagenta-4
BgTestColor=kBlack

histBgTrain.SetLineColor(BgTrainColor)
histBgTest.SetLineColor(BgTestColor)
histBgTrain.SetMarkerColor(BgTrainColor)
histBgTest.SetMarkerColor(BgTestColor)
histBgTrain.SetMarkerStyle(4)
histBgTest.SetMarkerStyle(4)

histSigTrain.GetYaxis().SetRangeUser(0.0, 300.0)
histSigTrain.GetXaxis().SetTitle("Classifier output probability")
histSigTrain.GetYaxis().SetTitle("Normalized events")
histSigTrain.GetYaxis().SetTitleOffset(1.18)
histSigTrain.DrawNormalized("E")
histSigTest.DrawNormalized("E SAME")
histBgTrain.DrawNormalized("E SAME")
histBgTest.DrawNormalized("E SAME")

legend = TLegend(0.3, 0.6, 0.7, 0.9)
legend.AddEntry(histSigTrain, "Train signal ", "pl")
legend.AddEntry(histSigTest, "Test signal", "pl")
legend.AddEntry(histBgTrain, "Train background", "pl")
legend.AddEntry(histBgTest, "Test background", "pl")
c1 = TCanvas()
legend.Draw()
print("Kolmogorov Test for signal: {}".format(histSigTrain.KolmogorovTest(histSigTest,"D")))
print("Chi2 Test for signal: {}".format(histSigTrain.Chi2Test(histSigTest)))
print("Kolmogorov Test for background: {}".format(histBgTest.KolmogorovTest(histBgTrain,"D")))
print("Chi2 Test for background: {}".format(histBgTrain.Chi2Test(histBgTest)))
######

#raw_input()
canvas1.SaveAs("plots/event_probability_%s.pdf" % cType)
