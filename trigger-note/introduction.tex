\section{Introduction}
\label{sec:introduction}

The $\gamma\gamma$ final state is interesting for a variety of reasons. The decay of a \Bs meson
to two photons remains unobserved and is described by an annihilation topology.
It is sensitive to contributions from physics beyond the Standard Model (SM) including a fourth generation~\cite{Huo:2003cj}, an extended Higgs sector~\cite{Aliev:1998va} and SUSY~\cite{Gemintern:2004bw}.
Previous measurements by the \belle and \babar collaborations have set limits of $\BR(\Bstogg) < 8.7 \times 10^{-6}$ at
$90\,\%$ confidence level (CL)~\cite{Dutta:2014sxo} and $\BR(\Bdtogg) < 3.3\times 10^{-7}$ at $90\,\%$ CL~\cite{delAmoSanchez:2010bx},
which are significantly above the SM predictions of $\BR(\Bstogg) \sim (2-37)\times 10^{-7}$ and $\BR(\Bdtogg) \sim (1-10)\times 10^{-8}$~\cite{Bosch:2002bv}.

%%% MORE ALP intro here %%%%%
%\textcolor{red}{***Xabi***, ALPs needed}
In addition to the decay of \Bs mesons to two photons, undiscovered particles known as Axion-Like Particles (ALPs)
are also the goal of the triggers described in this note.
ALPs are Pseudo Nambu Golstone Bosons, associated to spontaneously broken approximate symmetries, which appear in several models and can solve many SM problems \cite{CidVidal:2018blh}.
Probing very small coupling of ALPS to the SM sets indirectly constraints to the New Physics (NP) scale.
In the SM sector, ALPs couple to gluons (which allows their production at the LHC) or photons (which can be used for their discovery). The mass of ALPs can be arbitrarily below the NP scale. In particular, for ALPs with a mass in the range between 3-10 \gevcc, LHCb has unique sensitivity for their discovery \cite{CidVidal:2018blh}.


The maximum rate at which events can be
read out of the detector is imposed by the front-end electronics and
corresponds to a rate of 1.1\,MHz. In order to determine which events
are kept, hardware triggers based on field-programmable gate arrays
are used with a fixed latency of $4\,\mu {\rm s}$. Information from
the \ecal, \hcal, and muon stations is used in separate \lone
triggers. All events selected by \lone are transferred to the High Level
Trigger (HLT).
The HLT is a software application, executed on an event filter farm, that is implemented 
in the same Gaudi framework~\cite{Barrand:2001ny} as the software used for the offline reconstruction. 
It consists of 2 levels: an initial selection of high energy and/or displaced single- or double- particle signatures
(HLT1) and a second level, in which more complex searches are performed (HLT2).

The study of these purely neutral modes at \lhcb is challenging, but the use of photon conversions, 
which happen for around $25\,\%$ of photons, provides handles to reduce the background levels.
With offline selections already in place since Run 1, this note describes the trigger strategy adopted in Run 2, where a set of trigger lines were
introduced to select the $\gamma\gamma$ signature for the case of zero, one, and two photon conversions.\footnote{In this note, these will be labelled as \emph{0CV}, \emph{1CV} (both \emph{LL} and \emph{DD}, corresponding to \emph{long} and \emph{downstream} conversions) and \emph{2CV}, respectively.}
The purpose of this Note is to document an update to the previous strategy~\cite{Benson:2314368},
in which a selection strategy was put in place for \emph{0CV}, \emph{1CV LL}, \emph{1CV DD}, and \emph{2CV}.
In this note, updates to accommodate a wider mass range are described for the \emph{0CV}, \emph{1CV LL}, and \emph{1CV DD}
topologies. The \emph{2CV} selection remains as is described in Ref.~\cite{Benson:2314368}.

Section~\ref{sec:hlt1} describes the changes made to the HLT1 level to accommodate the wider mass range.
Sections~\ref{sec:samples}, \ref{sec:classifier}, and \ref{sec:perf} describe the samples used to train the
neural network, the training of the neural network, and the performance of the neural network, respectively.
The method used to incorporate the neural network into the central software of the experiment is described
in Section~\ref{sec:implementation}.

Throughout the note, efficiencies are evaluated with respect to a loose offline selection, detailed in Table~\ref{tab:preselection}, inspired by those applied in the analysis of other radiative decays.
\begin{table}[bh]
    \centering
    \caption{Loose offline selection for \decay{\Bs}{\gamma\gamma} used as a baseline for the efficiency determination.
    The fraction of events in each category after this loose selection with respect to the total events that pass it is shown in the last row.\label{tab:preselection}}
    \begin{tabular}{lcccc}
        \toprule
Variable                                   & \emph{0CV}   & \emph{1CV LL} & \emph{1CV DD} \\\midrule
  Calo $\gamma$ CL                         & $>0.3$       & $>0.3$        & $>0.3$        \\
  Calo $\gamma$ $p$ $[\!\gevc]$            & $>6$         & $>6$          & $>6$          \\
  Calo $\gamma$ \et $[\!\gev]$             & $>3 $        & $>3$          & $>3$          \\ \midrule
  Converted $\gamma$ \pt $[\!\gevc]$       & --           & $>2.0$        & $>2.0$        \\
  Converted $\gamma$ $M$ $[\!\mevcc]$      & --           & $<60$         & $<60$         \\
  Converted $\gamma$ \chisqip              & --           & $>4$          & $>0$          \\ \midrule
  $\sum p_{\textrm{T},\gamma}$  $[\!\gev]$ & $>6.5$       & $>5.5$        & $>5.5$        \\ \midrule
  $\Bs\, \pt~[\!\gevc]$                    & $>3.0$       & $>3.0$        & $>3.0$        \\
  $\Bs$ \chisqvtx                          & --           & --            & --            \\
  Fraction of signal                       & $83.4\%$     & $4.3\%$       & $11.7\%$      \\
    \bottomrule
    \end{tabular}
\end{table}


% EOF
