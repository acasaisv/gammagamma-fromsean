# General configuration
from Configurables import L0App
app = L0App()
app.Simulation = True
app.ReplaceL0Banks = True
app.outputFile = ""
app.EvtMax = 10000
app.TCK = '0x1801'

outputdir = "/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote"
app.DDDBtag   = "dddb-20150526"
app.CondDBtag = "sim-20161124-2-vc-md100"
app.DataType = "2016"

from Configurables import EventSelector
EventSelector().PrintFreq = 100

# InputCopyStream
from GaudiConf import IOHelper
from Configurables import InputCopyStream
input_copy = IOHelper().outputAlgs("%s/%s_L0_0x1801.ldst" % (outputdir, "ALP15"), InputCopyStream('CopyStream'), writeFSR = False)[0]

lst = [
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_1_L0.digi",
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_2_L0.digi",
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_3_L0.digi",
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_4_L0.digi",
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_5_L0.digi",
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_6_L0.digi",
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_7_L0.digi",
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15GeVetacut_pythia8_8_L0.digi"
]
from GaudiConf import IOHelper
IOHelper().inputFiles([l for l in lst])

# Juggle raw event
from Configurables import GaudiSequencer
from Gaudi.Configuration import ApplicationMgr

# Use RecEventTime to avoid problems with pre-juggled ODIN banks
from Configurables import DecodeRawEvent
DecodeRawEvent().EvtClockBank=""
from Configurables import EventClockSvc
clockSvc = EventClockSvc(EventTimeDecoder = 'RecEventTime/RecEventTime:PUBLIC')

# Timing table to make sure things work as intended
from Configurables import AuditorSvc, LHCbTimingAuditor
ApplicationMgr().AuditAlgorithms = 1
if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
    ApplicationMgr().ExtSvc.append('AuditorSvc')
AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))

# Output writers
# L0 filter
#from Configurables import L0DUFromRawAlg
#l0du_alg = L0DUFromRawAlg()
from Configurables import LoKiSvc
LoKiSvc().Welcome = False
from Configurables import LoKi__L0Filter
filter_code = "|".join(["L0_CHANNEL('%s')" % chan for chan in ['Electron','Photon','Hadron','Muon','DiMuon']])
l0_filter = LoKi__L0Filter('L0Filter', Code = filter_code)
# MDF writer
from Configurables import LHCb__MDFWriter as MDFWriter
mdf_writer = MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
Connection = 'file://%s/%s_L0_0x1801.ldst' % (outputdir, "ALP15") )
# Sequence
writer_seq = GaudiSequencer('WriterSeq')
writer_seq.Members = [l0_filter, mdf_writer, input_copy]
ApplicationMgr().OutStream = [writer_seq]
