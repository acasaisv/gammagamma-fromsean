# General configuration
from Configurables import L0App
app = L0App()
app.Simulation = True
app.ReplaceL0Banks = True
app.outputFile = ""
app.EvtMax = 10000
app.TCK = '0x1801'

outputdir = "/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote"
app.DataType = '2015'
app.DDDBtag  = "dddb-20140729"
app.CondDBtag = "sim-20140730-vc-md100"
lst = [
"root://eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/s/sbenson/2018_01/195877/195877390/Bs2gammagamma_L0_0x160D.ldst",
"root://whale3.grid.sara.nl:1094/pnfs/grid.sara.nl/data/lhcb/user/s/sbenson/2018_01/195877/195877387/Bs2gammagamma_L0_0x160D.ldst",
"root://eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/s/sbenson/2018_01/195877/195877388/Bs2gammagamma_L0_0x160D.ldst",
"root://f01-080-123-e.gridka.de:1094/pnfs/gridka.de/lhcb/user/s/sbenson/2018_01/195877/195877386/Bs2gammagamma_L0_0x160D.ldst",
]
from GaudiConf import IOHelper
IOHelper().inputFiles([l for l in lst])


from Configurables import EventSelector
EventSelector().PrintFreq = 100

# InputCopyStream
from GaudiConf import IOHelper
from Configurables import InputCopyStream
input_copy = IOHelper().outputAlgs("%s/%s_L0_0x1801.ldst" % (outputdir, "Bs"), InputCopyStream('CopyStream'), writeFSR = False)[0]

# Juggle raw event
from Configurables import GaudiSequencer
from Gaudi.Configuration import ApplicationMgr

# Use RecEventTime to avoid problems with pre-juggled ODIN banks
from Configurables import DecodeRawEvent
DecodeRawEvent().EvtClockBank=""
from Configurables import EventClockSvc
clockSvc = EventClockSvc(EventTimeDecoder = 'RecEventTime/RecEventTime:PUBLIC')

# Timing table to make sure things work as intended
from Configurables import AuditorSvc, LHCbTimingAuditor
ApplicationMgr().AuditAlgorithms = 1
if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
    ApplicationMgr().ExtSvc.append('AuditorSvc')
AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))

# Output writers
# L0 filter
#from Configurables import L0DUFromRawAlg
#l0du_alg = L0DUFromRawAlg()
from Configurables import LoKiSvc
LoKiSvc().Welcome = False
from Configurables import LoKi__L0Filter
filter_code = "|".join(["L0_CHANNEL('%s')" % chan for chan in ['Electron','Photon','Hadron','Muon','DiMuon']])
l0_filter = LoKi__L0Filter('L0Filter', Code = filter_code)
# MDF writer
from Configurables import LHCb__MDFWriter as MDFWriter
mdf_writer = MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
Connection = 'file://%s/%s_L0_0x1801.ldst' % (outputdir, "Bs") )
# Sequence
writer_seq = GaudiSequencer('WriterSeq')
writer_seq.Members = [l0_filter, mdf_writer, input_copy]
ApplicationMgr().OutStream = [writer_seq]
