import math
from ROOT import TVector3, TLorentzVector

mu_m = 105.658
pi_m = 139.570
el_m = 0.511

def prepPlot(_ax, xtitle, ytitle):
    _ax.grid(False)
    _ax.set_xlabel(xtitle)
    _ax.set_ylabel(ytitle)
    _ax.set_title("")

def toFourVector(m, px, py, pz):
    vec = TVector3(px, py, pz)
    return TLorentzVector(vec, math.sqrt(m*m + vec.Mag2()))

def getEta(px, py, pz):
    return TVector3(px, py, pz).Eta()

def corrMass(beginV, endV, L_partial):
    # beginV: [x,y,x]
    # endV: [x,y,z]
    # L_partial: [[m_1,x_1,y_1,z_1],...]
    
    vec_list = []
    for l in L_partial:
        # print('%s,%s,%s,%s' % (l[0],l[1],l[2],l[3]))
        vec_list.append(toFourVector(l[0], l[1], l[2], l[3]))
    partial = vec_list[0]
    for n in range(1,len(vec_list)):
        partial += vec_list[n]
    partialFlight = TVector3(endV[0], endV[1], endV[2])-TVector3(beginV[0], beginV[1], beginV[2])
    dot = partial.Vect().Unit().Dot(partialFlight)
    ptMiss = partial.Vect() - partialFlight * partial.Vect().Dot(partialFlight) * (1.0 / partialFlight.Mag2())
    ptMiss = ptMiss.Mag()
    return math.sqrt( max(partial.M(),0.0)**2 + ptMiss**2 ) + ptMiss
    
def addCorrM(df):
    df['corrM'] = df.apply(lambda x: corrMass([x['B0_OWNPV_X'], x['B0_OWNPV_Y'], x['B0_OWNPV_Z']]
                                                , [x['B0_ENDVERTEX_X'], x['B0_ENDVERTEX_Y'], x['B0_ENDVERTEX_Z']]
                                                , [[mu_m, x['muplus_PX'], x['muplus_PY'], x['muplus_PZ']]
                                                   , [mu_m, x['muminus_PX'], x['muminus_PY'], x['muminus_PZ']]
                                                   , [pi_m, x['piplus_PX'], x['piplus_PY'], x['piplus_PZ']]
                                                   , [pi_m, x['piminus_PX'], x['piminus_PY'], x['piminus_PZ']]
                                               ]), axis=1)
    return df
def addCorrM_2(df):
    df['corrM_2'] = df.apply(lambda x: corrMass([x['B0_OWNPV_X'], x['B0_OWNPV_Y'], x['B0_OWNPV_Z']]
                                                , [x['B0_ENDVERTEX_X'], x['B0_ENDVERTEX_Y'], x['B0_ENDVERTEX_Z']]
                                                , [[pi_m, x['piplus_PX'], x['piplus_PY'], x['piplus_PZ']]
                                                   , [pi_m, x['piminus_PX'], x['piminus_PY'], x['piminus_PZ']]
                                               ]), axis=1)
    return df

def addVars(df):
    df['max_pt'] = df.apply(lambda x: max([x['muplus_PT'], x['muminus_PT'], x['piplus_PT'], x['piminus_PT']]), axis=1)
    df['max_CHI2'] = df.apply(lambda x: max([x['muplus_TRACK_CHI2NDOF'], x['muminus_TRACK_CHI2NDOF'], x['piplus_TRACK_CHI2NDOF'], x['piminus_TRACK_CHI2NDOF']]), axis=1)
    df['min_lepIPCHI2'] = df.apply(lambda x: min([x['muplus_IPCHI2_OWNPV'], x['muminus_IPCHI2_OWNPV']]), axis=1)
    df['min_hadIPCHI2'] = df.apply(lambda x: min([x['piplus_IPCHI2_OWNPV'], x['piminus_IPCHI2_OWNPV']]), axis=1)
    df['min_ProbNNmu'] = df.apply(lambda x: min([x['muplus_ProbNNmu'], x['muminus_ProbNNmu']]), axis=1)
    df['min_eta'] = df.apply(lambda x: min([getEta(x['muplus_PX'], x['muplus_PY'], x['muplus_PZ'])
                                                    , getEta(x['muminus_PX'], x['muminus_PY'], x['muminus_PZ'])
                                                   , getEta(x['piplus_PX'], x['piplus_PY'], x['piplus_PZ'])
                                                   , getEta(x['piminus_PX'], x['piminus_PY'], x['piminus_PZ'])])
                                     , axis=1)
    return df

def addVars(df):
    df['max_pt'] = df.apply(lambda x: max([x['muplus_PT'], x['muminus_PT'], x['piplus_PT'], x['piminus_PT']]), axis=1)
    df['max_CHI2'] = df.apply(lambda x: max([x['muplus_TRACK_CHI2NDOF'], x['muminus_TRACK_CHI2NDOF'], x['piplus_TRACK_CHI2NDOF'], x['piminus_TRACK_CHI2NDOF']]), axis=1)
    df['min_lepIPCHI2'] = df.apply(lambda x: min([x['muplus_IPCHI2_OWNPV'], x['muminus_IPCHI2_OWNPV']]), axis=1)
    df['min_hadIPCHI2'] = df.apply(lambda x: min([x['piplus_IPCHI2_OWNPV'], x['piminus_IPCHI2_OWNPV']]), axis=1)
    df['min_ProbNNmu'] = df.apply(lambda x: min([x['muplus_ProbNNmu'], x['muminus_ProbNNmu']]), axis=1)
    df['min_eta'] = df.apply(lambda x: min([getEta(x['muplus_PX'], x['muplus_PY'], x['muplus_PZ'])
                                                    , getEta(x['muminus_PX'], x['muminus_PY'], x['muminus_PZ'])
                                                   , getEta(x['piplus_PX'], x['piplus_PY'], x['piplus_PZ'])
                                                   , getEta(x['piminus_PX'], x['piminus_PY'], x['piminus_PZ'])])
                                     , axis=1)
    return df

def addPNNminmax(df):
    df['min_ProbNNk'] = df.apply(lambda x: min([x['piplus_ProbNNk'], x['piminus_ProbNNk']]), axis=1)
    df['max_ProbNNk'] = df.apply(lambda x: max([x['piplus_ProbNNk'], x['piminus_ProbNNk']]), axis=1)
    return df

def scanPoint(cutVal, sig, bkg):
    totSig = len(sig)
    totBKG = len(bkg)
    sig_pass = len(sig.loc[sig['prob'] > cutVal])
    bkg_rej = len(bkg.loc[bkg['prob'] < cutVal])
    bkg_pass = len(bkg.loc[bkg['prob'] > cutVal])
    eff_sig = float(sig_pass)/float(totSig)
    rej_bkg = float(bkg_rej)/float(totBKG)
    return eff_sig, rej_bkg, sig_pass, bkg_pass

def FoM(s, b):
    if s==0:
        return 0.0
    s = float(s)
    b = float(b)
    return s/math.sqrt(s+b)

def addProb(df, cl, sc):
    df['prob']= df.apply(lambda x: float(cl.predict_proba(sc.transform(x.values.reshape(1, -1)))[0][1]), axis=1)
    return df
