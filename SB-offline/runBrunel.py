from Configurables import Brunel
#Brunel().DDDBtag = "dddb-20150724"
#Brunel().CondDBtag = "sim-20161124-2-vc-md100"
#Brunel().DataType = "2016"
#from Configurables import RootCnvSvc
#RootCnvSvc().GlobalCompression = "ZLIB:1"
from Configurables import Brunel

Brunel().InputType = "DIGI" # implies also Brunel().Simulation = True
Brunel().WithMC    = True   # implies also Brunel().Simulation = True
Brunel().Simulation    = True
Brunel().PrintFreq = 100

# ParticleGun
Brunel().SpecialData = ['pGun']

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False 
