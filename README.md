** Purpose

This GIT repo provides the anaysis scripts used in the offline selection.

### SB-offline

Folder containing a collection of scripts used to run the entire simulation chain.

See runChain_complete_pStrip.sh

### offline_keras_training

Folder containing classifiers and the notebooks that made them.

It should be noted that the data used to train the networks consists of pandas
dataframes. In this folder leaves.py accomplishes the task of creating the data
frames from the ROOT files.
The resulting dataframes can be found in `/eos/lhcb/wg/RD/Bs2GammaGamma/LHCb-INT-2019-010/`.

### classifiers/notebooks

A summary of the resulting classifiers

### Documentation

int-note: The internal note describing the Keras offline classifier training and performance
trigger-note: An old version of the gamma gamma trigger and prospects paper, before moving to [overleaf](https://www.overleaf.com/project/5c79368f0c4fc534eeeb9e5b):
