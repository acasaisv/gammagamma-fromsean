##############################################################################
# Database tags for Sim09b for 2016
##############################################################################
from Configurables import LHCbApp
LHCbApp().DDDBtag = "dddb-20150724"
LHCbApp().CondDBtag = "sim-20161124-2-vc-mu100"
LHCbApp().DataType = "2016"
