from Configurables import DaVinci
# Necessary DaVinci parameters #################
DaVinci().Simulation   = True
DaVinci().SkipEvents = 0
DaVinci().EvtMax = -1
DaVinci().DataType      = "2016"
DaVinci().TupleFile = 'Bs2GG.root'
DaVinci().HistogramFile = "dummy.root"
DaVinci().InputType = 'DST'
DaVinci().RootCompressionLevel = "ZLIB:1"
