from Configurables import Boole
# just instantiate the configurable
theApp = Boole()
from Configurables import BooleInit
BooleInit().SetOdinRndTrigger = True
# enable spillover 
#from Configurables import Boole
#Boole().UseSpillover = True
#   
#Boole().SpilloverPaths = ["PrevPrev", "Prev", "Next", "NextNext"]

#Boole().DDDBtag = "dddb-20150724"
#Boole().CondDBtag = "sim-20161124-2-vc-md100"
#Boole().DataType = "2016"

from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "ZLIB:1"

#Boole().PrintFreq = 100
from Configurables import BooleInit
BooleInit().PrintFreq = 100
