# File for setting Beam conditions for July to September 2012 4 TeV
# taken in 2011 with magnet down.
# Beam 4.0 TeV, beta* = 3m , emittance(normalized) ~ 2.5 micron
#
# Requires Gauss v40r0 or higher.
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam4000GeV-md100-Apr2012-nu2.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              MC11a-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from Configurables import EventClockSvc, FakeEventTime
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
#-- nu=2.5 (i.e. mu=1.76, L/bunch below)
Gauss().Luminosity        = 0.302*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
# CrossingRate = 11.245*SystemOfUnits.kilohertz used internally
Gauss().TotalCrossSection = 93.2*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
Gauss().BunchRMS = 53.034*SystemOfUnits.mm

Gauss().InteractionPosition = [ 0.0*SystemOfUnits.mm ,
                                0.0*SystemOfUnits.mm ,
                                27.9*SystemOfUnits.mm ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance (e_norm ~ 2 microns)
Gauss().BeamMomentum      = 4.0*SystemOfUnits.TeV
Gauss().BeamHCrossingAngle = -0.395*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.000*SystemOfUnits.mrad
Gauss().BeamEmittance     = 0.0025*SystemOfUnits.mm
Gauss().BeamBetaStar      = 3.0*SystemOfUnits.m
Gauss().BeamLineAngles    = [0.0, 0.0]

# Introduce a vertical crossing angle
def setVCrossingAngle():
    from Configurables import GenInit
    genInit = GenInit( "GaussGen" )
    genInit.VerticalCrossingAngle   = 0.100*SystemOfUnits.mrad

from Gaudi.Configuration import *
appendPostConfigAction( setVCrossingAngle )


#
#--Starting time, all events will have the same
#--Can be used for beam conditions: YEBM (year,energy,bunch-spacing,field)
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 2401.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0

