. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/InstallArea/scripts/LbLogin.sh
cd /afs/cern.ch/work/s/sbenson/BsGammaGamma/Trigger/ALPs/note_stats

# Run Bs
lb-run moore/v28r3 gaudirun.py ReRun_L0_MgDn.py 2>&1 | tee "Bs_L0.log"
#lb-run moore/v28r3 gaudirun.py hlt1.py 2>&1 | tee "Bs_hlt1.log"
#lb-run moore/v28r3 gaudirun.py hlt2.py 2>&1 | tee "Bs_hlt2.log"

# Run ALP5
lb-run moore/v28r3 gaudirun.py ReRun_L0_alp5.py 2>&1 | tee "alp5_L0.log" 
#lb-run moore/v28r3 gaudirun.py hlt1_alp5.py 2>&1 | tee "alp5_hlt1.log"
#lb-run moore/v28r3 gaudirun.py hlt2_alp5.py 2>&1 | tee "alp5_hlt2.log"
# Run ALP10
lb-run moore/v28r3 gaudirun.py ReRun_L0_alp10.py 2>&1 | tee "alp10_L0.log" 
#lb-run moore/v28r3 gaudirun.py hlt1_alp10.py 2>&1 | tee "alp10_hlt1.log"
#lb-run moore/v28r3 gaudirun.py hlt2_alp10.py 2>&1 | tee "alp10_hlt2.log"
# Run ALP15
lb-run moore/v28r3 gaudirun.py ReRun_L0_alp15.py 2>&1 | tee "alp15_L0.log" 
#lb-run moore/v28r3 gaudirun.py hlt1_alp15.py 2>&1 | tee "alp15_hlt1.log"
#lb-run moore/v28r3 gaudirun.py hlt2_alp15.py 2>&1 | tee "alp15_hlt2.log"
