##############################################################################
# File for running Gauss on 2012 detector configuration
##############################################################################

from Configurables import Gauss, LHCbApp
Gauss().DataType  = "2012"

from Gauss.Configuration import *
GaussGen = GenInit("GaussGen")
import os
GaussGen.FirstEventNumber = 1
if 'RUNNUMBER' in os.environ:
    GaussGen.RunNumber        = os.environ['RUNNUMBER']
else:
    GaussGen.RunNumber        = 1010

#--Number of events
if 'EVTMAX' in os.environ:
    nEvts = os.environ['EVTMAX']
else:
    nEvts = 10
LHCbApp().EvtMax = nEvts
