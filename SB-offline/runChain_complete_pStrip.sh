#!/bin/bash

source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc49-opt
export CMTCONFIG=x86_64-slc6-gcc49-opt

SJNUM=$(echo $PBS_JOBNAME | sed 's/[^0-9]*//g')
seed=$SJNUM
mainDir="/project/bfys/sbenson/PG/AsymmetryTools/projects/ParticleGun/"
output="/data/bfys/sbenson/PG"

evts=200
#evttype=11114017
evttype=13100212
polarity="MagUp"
mode=GammaGamma
year="2016"

# Mag up goes to 723
export RUNNUMBER=$(( $seed + 16951 ))
export EVTMAX=$evts
export MAINDIR=$mainDir

export APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r328/options
export LBBCVEGPYROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v49r5/Gen/LbBcVegPy
export DECFILESROOT=/project/bfys/sbenson/PG/AsymmetryTools/projects/ParticleGun/GaussDev_v49r5/Gen/DecFiles

if [ $year == "2012" ]
then 
  if [ $polarity == "MagDown" ]
  then 
  gaussOpts=$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py
  fi
  if [ $polarity == "MagUp" ]
  then 
  gaussOpts=$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py
  fi
fi
if [ $year == "2016" ]
then 
  if [ $polarity == "MagDown" ]
  then 
  gaussOpts="$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py $APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py"
  fi
  if [ $polarity == "MagUp" ]
  then 
  gaussOpts="$APPCONFIGOPTS/Gauss/Beam6500GeV-mu100-2016-nu1.6.py $APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py"
  fi
fi

echo "================================================================================================================================"
echo "Starting on : $(date)"
echo "Running on node : $(hostname)"
echo "Current directory : $(pwd)"
echo "Current job ID : $SJNUM"
echo "Current job name : $Job_Name"
echo "Output directory: $output"

mkdir ${output}/${PBS_JOBNAME}_${polarity}_${mode}_${year}
cd ${output}/${PBS_JOBNAME}_${polarity}_${mode}_${year}

# GAUSS
gaussOut=Gauss-${evttype}-${evts}ev-$( date +%Y%m%d ).sim
${mainDir}/GaussDev_v49r5/run gaudirun.py ${gaussOpts} $APPCONFIGOPTS/Gauss/RICHRandomHits.py $APPCONFIGOPTS/Gauss/NoPacking.py $DECFILESROOT/options/${evttype}.py $LBBCVEGPYROOT/options/BcVegPyPythia8.py $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py ${mainDir}/options/${year}-${polarity}-conditions.py ${mainDir}/options/Gauss_DataType_${year}.py

# move gauss output to expected name
mv ${gaussOut} Gauss.sim

# BOOLE
lb-run Boole/v30r2 gaudirun.py ${mainDir}/options/runBoole.py ${mainDir}/options/${year}-${polarity}-conditions.py ${mainDir}/options/Gauss-Data.py

# TRIGGER
bash ${mainDir}/options/runChain_full_trig1.sh ${mainDir} ${polarity} ${year} 
bash ${mainDir}/options/runChain_full_trig2.sh ${mainDir} ${polarity} ${year}

# RECO
lb-run Brunel/v50r2 gaudirun.py ${mainDir}/options/runBrunel.py ${mainDir}/options/${year}-${polarity}-conditions.py ${mainDir}/options/Hlt-Data.py 

# STRIPPING
lb-run DaVinci/v41r4 gaudirun.py ${mainDir}/options/stripping/_batch_tuple.py ${mainDir}/options/stripping/DVSetup-MC${year}.py ${mainDir}/options/stripping/MCTags-${polarity}.py ${mainDir}/options/Stripping-Data.py

cp -f Bs2GG.root "${output}/Stripped_${PBS_JOBNAME}_${year}_${mode}_${polarity}_${SJNUM}.root"

rm -r ${output}/${PBS_JOBNAME}_${polarity}_${mode}_${year}
