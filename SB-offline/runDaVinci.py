from Configurables import DaVinci, GaudiSequencer
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

DaVinci().DataType = "2016"

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdAllLooseKaons as kaons
from StandardParticles import StdAllLoosePions as pions
from StandardParticles import StdNoPIDsDownPions as downPions
from StandardParticles import StdLooseKsLL, StdLooseKsDD
combineDpToKPiPi = CombineParticles()
combineDpToKPiPi.DaughtersCuts = { "pi+" : "(P > 2.*GeV) & (PT>50*MeV)" , "K+" : "(P > 2.*GeV) & (PT>50*MeV)"  }
combineDpToKPiPi.MotherCut = "(VFASPF(VCHI2)<25.0) & (in_range(%(D_Mass_MIN)s,M,%(D_Mass_MAX)s))" % {"D_Mass_MIN": 1700*MeV, "D_Mass_MAX" : 2100*MeV }
#combineDpToKPiPi.MotherCut = "(VFASPF(VCHI2)<25.0) " 
combineDpToKPiPi.DecayDescriptors = [ "[D+ -> K- pi+ pi+]cc" ]
selectDpToKPiPi = Selection( "SelectDpToKPiPi", Algorithm = combineDpToKPiPi, RequiredSelections = [kaons, pions] )
sequenceDpToKPiPi = SelectionSequence('SequenceDpToKPiPi' , TopSelection = selectDpToKPiPi)

StdAllLooseKsLL = CombineParticles()
StdAllLooseKsLL.DecayDescriptor = "KS0 -> pi+ pi-" 
StdAllLooseKsLL.DaughtersCuts = { "pi+" : "(P > 2.*GeV)" } 
StdAllLooseKsLL.CombinationCut = "(ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(25, '')) "
StdAllLooseKsLL.MotherCut = "(ADMASS('KS0') < 55.*MeV) & (VFASPF(VCHI2) < 25.)"
selectKsLL = Selection( "SelectKsLL", Algorithm = StdAllLooseKsLL, RequiredSelections = [pions] )
sequenceKsLL = SelectionSequence('SequenceKsLL' , TopSelection = selectKsLL)

StdAllLooseKsDD = CombineParticles()
StdAllLooseKsDD.DecayDescriptor = "KS0 -> pi+ pi-" 
StdAllLooseKsDD.DaughtersCuts = { "pi+" : "(P > 2.*GeV)" } 
StdAllLooseKsDD.CombinationCut = "(ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(25, '')) "
StdAllLooseKsDD.MotherCut = "(ADMASS('KS0') < 55.*MeV) & (VFASPF(VCHI2) < 25.)"
selectKsDD = Selection( "SelectKsDD", Algorithm = StdAllLooseKsDD, RequiredSelections = [downPions] )
sequenceKsDD = SelectionSequence('SequenceKsDD' , TopSelection = selectKsDD)


combineDpToKsLLPi = CombineParticles()
combineDpToKsLLPi.DaughtersCuts = { "pi+" : "(PT>50*MeV)" , "KS0" : "PT>50*MeV"  }
combineDpToKsLLPi.MotherCut = "(VFASPF(VCHI2)<25.0) & (in_range(%(D_Mass_MIN)s,M,%(D_Mass_MAX)s))" % {"D_Mass_MIN": 1700*MeV, "D_Mass_MAX" : 2100*MeV }
combineDpToKsLLPi.DecayDescriptors = [ "[D+ -> KS0 pi+]cc" ]
selectDpToKsLLPi = Selection( "SelectDpToKsLLPi", Algorithm = combineDpToKsLLPi, RequiredSelections = [selectKsLL, pions] )
sequenceDpToKsLLPi = SelectionSequence('SequenceDpToKsLLPi' , TopSelection = selectDpToKsLLPi)

combineDpToKsDDPi = CombineParticles()
combineDpToKsDDPi.DaughtersCuts = { "pi+" : "(PT>50*MeV)" , "KS0" : "PT>50*MeV"  }
combineDpToKsDDPi.MotherCut = "(VFASPF(VCHI2)<25.0) & (in_range(%(D_Mass_MIN)s,M,%(D_Mass_MAX)s))" % {"D_Mass_MIN": 1700*MeV, "D_Mass_MAX" : 2100*MeV }
combineDpToKsDDPi.DecayDescriptors = [ "[D+ -> KS0 pi+]cc" ]
selectDpToKsDDPi = Selection( "SelectDpToKsDDPi", Algorithm = combineDpToKsDDPi, RequiredSelections = [selectKsDD, pions] )
sequenceDpToKsDDPi = SelectionSequence('SequenceDpToKsDDPi' , TopSelection = selectDpToKsDDPi)


tuples = [{"Name"   : "KPiPi",
           "Inputs" : [sequenceDpToKPiPi.outputLocation()],
           "Decay"  : "[D+ -> ^K- ^pi+ ^pi+]CC ",
           "DecayBranches" : {"D":"[D+ -> K- pi+ pi+]CC ",
                              "K" :"[D+ -> ^K- pi+ pi+]CC",
                              "Pi1" :"[D+ -> K- ^pi+ pi+]CC",
                              "Pi2" :"[D+ -> K- pi+ ^pi+]CC"},
           "DecayHead" : "D"},
          {"Name"   : "KsLLPi",
           "Inputs" : [sequenceDpToKsLLPi.outputLocation()],
           "Decay"  : "[D+ -> ^(KS0-> ^pi+ ^pi- ) ^pi+ ]CC ",
           "DecayBranches" : {"D":"[D+ -> (KS0 -> pi+ pi- ) pi+]CC ",
                              "KS":"[D+ -> ^(KS0 -> pi+ pi- ) pi+]CC ",
                              "Pi":"[D+ -> (KS0 -> pi+ pi- ) ^pi+]CC ",
                              "KS" :"[D+ -> ^(KS0 -> pi+ pi- ) pi+]CC ",
                              "Pi1" :"[D+ -> (KS0 -> ^pi+ pi- ) pi+]CC ",
                              "Pi2" :"[D+ -> (KS0 -> pi+ ^pi- ) pi+]CC "},
           "DecayHead" : "D"},
           {"Name"   : "KsDDPi",
            "Inputs" : [sequenceDpToKsDDPi.outputLocation()],
            "Decay"  : "[D+ -> ^(KS0-> ^pi+ ^pi- ) ^pi+ ]CC ",
            "DecayBranches" : {"D":"[D+ -> (KS0 -> pi+ pi- ) pi+]CC ",
                               "KS":"[D+ -> ^(KS0 -> pi+ pi- ) pi+]CC ",
                               "Pi":"[D+ -> (KS0 -> pi+ pi- ) ^pi+]CC ",
                               "KS" :"[D+ -> ^(KS0 -> pi+ pi- ) pi+]CC ",
                               "Pi1" :"[D+ -> (KS0 -> ^pi+ pi- ) pi+]CC ",
                               "Pi2" :"[D+ -> (KS0 -> pi+ ^pi- ) pi+]CC "},
            "DecayHead" : "D"}
           ]
          
mcTuples = [{"Name"   : "KPiPi",
             "Decay"  : "[D+ ==> ^K- ^pi+ ^pi+]CC ",
             "DecayBranches" : {"D":"[D+ ==> K- pi+ pi+]CC ",
                                "K" :"[D+ ==> ^K- pi+ pi+]CC",
                                "Pi1" :"[D+ ==> K- ^pi+ pi+]CC",
                                "Pi2" :"[D+ ==> K- pi+ ^pi+]CC"},
             "DecayHead" : "D"},
            
            {"Name"   : "KsPi",
             "Decay"  : "[D+ ==> ^(KS0 -> ^pi+ ^pi- ...) ^pi+ ]CC ",
             "DecayBranches" : {"D":"[D+ ==> (KS0 -> pi+ pi- ...) pi+ ]CC ",
                                "KS":"[D+ ==> ^(KS0 -> pi+ pi- ...) pi+ ]CC ",
                                "Pi":"[D+ ==> (KS0 -> pi+ pi- ...) ^pi+ ]CC ",
                                "Pi1" :"[D+ ==> (KS0 -> ^pi+ pi- ...) pi+ ]CC ",
                                "Pi2" :"[D+ ==> (KS0 -> pi+ ^pi- ...) pi+ ]CC "
                                },
             "DecayHead" : "D"},
            {"Name"   : "KsPi2",
             "Decay"  : "[D+ ==> ^KS0 ^pi+ ]CC ",
             "DecayBranches" : {"D":"[D+ ==> pi+ ]CC ",
                                "KS":"[D+ ==> ^KS0  pi+ ]CC ",
                                "Pi":"[D+ ==> KS0  ^pi+ ]CC ",
                                },
             "DecayHead" : "D"}
             
           ]
tupleSeq = GaudiSequencer("TupleSeq")
tupleSeq.ModeOR = True
tupleSeq.ShortCircuit = False
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import DecayTreeTuple, TupleToolTrackInfo, TupleToolRecoStats,LoKi__Hybrid__Dict2Tuple, LoKi__Hybrid__DictOfFunctors
from Configurables import TupleToolMCTruth
from DecayTreeTuple.Configuration import *


for tuple in tuples:
    tup = DecayTreeTuple(tuple["Name"])
    
    tup.Inputs = tuple["Inputs"]
    tup.Decay = tuple["Decay"]
    
    tup.addBranches(tuple["DecayBranches"])
    
    #Refit the PV
    tup.ReFitPVs = False
    
    
    tl= [ "TupleToolKinematic", 
          "TupleToolEventInfo",
          "TupleToolMCBackgroundInfo",
          ]
    
    tup.ToolList += tl
    
    tupleToolTrackInfo = tup.addTupleTool(TupleToolTrackInfo, name='TupleToolTrackInfo')
    tupleToolTrackInfo.Verbose = False
    
    # RecoStats to filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tupleToolRecoStats = tup.addTupleTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tupleToolRecoStats.Verbose=True

    # MC information
    tupleToolMCTruth = tup.addTupleTool(TupleToolMCTruth, name='TupleToolMCTruth')
    tupleToolMCTruth.ToolList =  [ "MCTupleToolHierarchy", "MCTupleToolKinematic",  ]

    ##LIST OF LOKI VARIABLES
    from Configurables import LoKi__Hybrid__TupleTool
    hybridTool = tup.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Hybrid')
    hybridTool.Variables = {
        "ETA" : "ETA",
        "PHI" : "PHI"
        }

    # event tuple
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple = tup.addTupleTool(LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple"))
    LoKi_EvtTuple.VOID_Variables = { 
    	"LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
    	"LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
    	"LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
    	"LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
    	"LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
    	"LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
    	"LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
    	}   
    DictTuple = tup.D.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
    DictTuple.addTool(DTFDict, "DTF")
    DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
    DictTuple.NumVar = 19     # reserve a suitable size for the dictionaire
    DictTuple.DTF.constrainToOriginVertex = True
    DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors, "dict")
    DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
    DictTuple.DTF.dict.Variables = {
        "DTFDict_D0_M"             : "M",
        "DTFDict_D0_P"             : "P",
        "DTFDict_K_PX"             : "CHILD(1, PX)",
        "DTFDict_K_PY"             : "CHILD(1, PY)",
        "DTFDict_K_PZ"             : "CHILD(1, PZ)",
        "DTFDict_K_PT"             : "CHILD(1, PT)",
        "DTFDict_K_ETA"            : "CHILD(1, ETA)",
        "DTFDict_K_PHI"            : "CHILD(1, PHI)",
        "DTFDict_Pi1_PT"           : "CHILD(2, PT)",
        "DTFDict_Pi1_ETA"          : "CHILD(2, ETA)",
        "DTFDict_Pi1_PHI"          : "CHILD(2, PHI)",
        "DTFDict_D0_ETA"           : "ETA",
        "DTFDict_D0_PT"            : "PT",
        "DTFDict_D0_PHI"           : "PHI",
        "DTFDict_BPVDIRA"          : "BPVDIRA",
        "DTFDict_BPVLIFETIME"      : "BPVLTIME()"
        , "DTFDict_IPS" : "MIPCHI2DV(PRIMARY)"
        , "DTFDict_VFASPF_CHI2DOF" : "VFASPF(VCHI2/VDOF)"
        , "DTFDict_VFASPF_CHI2" : "VFASPF(VCHI2)"
        , "DTFDict_BPVIPCHI2" : "BPVIPCHI2()",
        "DTFDict_D0_VX_Z"          : "VFASPF(VZ)",
        "DTFDict_D0_VX_Y"          : "VFASPF(VY)",
        "DTFDict_D0_VX_X"          : "VFASPF(VX)",
        "DTFDict_VRedChi2"         : "VFASPF(VCHI2/VDOF)"
        }
    DictTupleTwo = tup.D.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTupleTwo")
    DictTupleTwo.addTool(DTFDict, "DTFTwo")
    DictTupleTwo.Source = "LoKi::Hybrid::DTFDict/DTFTwo"
    DictTupleTwo.NumVar = 19     # reserve a suitable size for the dictionaire
    DictTupleTwo.DTFTwo.constrainToOriginVertex = True
    DictTupleTwo.DTFTwo.daughtersToConstrain = ["D+"]
    DictTupleTwo.DTFTwo.addTool(LoKi__Hybrid__DictOfFunctors, "dict")
    DictTupleTwo.DTFTwo.Source = "LoKi::Hybrid::DictOfFunctors/dict"
    DictTupleTwo.DTFTwo.dict.Variables = {
        "DTFD0_D0_M"             : "M",
        "DTFD0_D0_P"             : "P",
        "DTFD0_K_PX"             : "CHILD(1, PX)",
        "DTFD0_K_PY"             : "CHILD(1, PY)",
        "DTFD0_K_PZ"             : "CHILD(1, PZ)",
        "DTFD0_K_PT"             : "CHILD(1, PT)",
        "DTFD0_K_ETA"            : "CHILD(1, ETA)",
        "DTFD0_K_PHI"            : "CHILD(1, PHI)",
        "DTFD0_Pi1_PT"           : "CHILD(2, PT)",
        "DTFD0_Pi1_ETA"          : "CHILD(2, ETA)",
        "DTFD0_Pi1_PHI"          : "CHILD(2, PHI)",
        "DTFD0_D0_ETA"           : "ETA",
        "DTFD0_D0_PT"            : "PT",
        "DTFD0_D0_PHI"           : "PHI",
        "DTFD0_BPVDIRA"          : "BPVDIRA",
        "DTFD0_BPVLIFETIME"      : "BPVLTIME()"
        , "DTFD0_IPS" : "MIPCHI2DV(PRIMARY)"
        , "DTFD0_VFASPF_CHI2DOF" : "VFASPF(VCHI2/VDOF)"
        , "DTFD0_VFASPF_CHI2" : "VFASPF(VCHI2)"
        , "DTFD0_BPVIPCHI2" : "BPVIPCHI2()",
        "DTFD0_D0_VX_Z"          : "VFASPF(VZ)",
        "DTFD0_D0_VX_Y"          : "VFASPF(VY)",
        "DTFD0_D0_VX_X"          : "VFASPF(VX)",
        "DTFD0_VRedChi2"         : "VFASPF(VCHI2/VDOF)"
        }
    
    tupleSeq.Members += [tup]

    
for tuple in mcTuples:
    mctuple = MCDecayTreeTuple( "MC" + tuple["Name"] )
    mctuple.Decay = tuple["Decay"]
    mctuple.addBranches(tuple["DecayBranches"])
    mctuple.ToolList = [ "MCTupleToolKinematic", "MCTupleToolReconstructed", "MCTupleToolDecayType", 
                        "MCTupleToolHierarchy", "MCTupleToolPID", 
                        "LoKi::Hybrid::MCTupleTool/LoKi_Photos", "TupleToolEventInfo" ]
    from Configurables import LoKi__Hybrid__MCTupleTool
    hybridTool = mctuple.addTupleTool('LoKi::Hybrid::MCTupleTool/LoKi_MCHybrid')
    hybridTool.Variables = {
        "ETA" : "MCETA",
        "PHI" : "MCPHI"
        }

    
    tupleSeq.Members += [mctuple]

from Configurables import PrintMCTree
pMC = PrintMCTree()
pMC.ParticleNames = [ "D+" , "D-" ]

userAlgos = [sequenceDpToKPiPi, sequenceKsDD, sequenceKsLL, sequenceDpToKsLLPi, sequenceDpToKsDDPi, tupleSeq]

DaVinci().HistogramFile = 'pGun_histos.root'
DaVinci().TupleFile = 'pGun_DT.root'
DaVinci().UserAlgorithms = userAlgos
DaVinci().InputType = "DST"
DaVinci().Simulation = True
DaVinci().PrintFreq = 1
DaVinci().Lumi       = False
from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
