import os
from root_pandas import read_root
import pandas as pd

def constr_list(f_in, level1, level2=None):
    ret_list = []
    with open(f_in) as f:
        for line in f:
            if 'calog' in line:
                if 'calog' not in level1.keys():
                    continue
                for l1 in level1['calog']:
                    ret_list.append(line.replace('calog',l1).rstrip('\n'))
            elif 'photon_conv' in line:
                if 'photon_conv' not in level1.keys():
                    continue
                for l1 in level1['photon_conv']:
                    ret_list.append(line.replace('photon_conv',l1).rstrip('\n'))
            elif 'pos_' in line:
                if not level2:
                    continue
                for l2 in level2:
                    ret_list.append(line.replace('pos',l2,1).rstrip('\n'))
            else:
                ret_list.append(line.rstrip('\n'))
    print('Returning the following columns:')
    print(ret_list)
    return ret_list

leaves_none = constr_list('gg_leaves.txt', {'calog' : ['photon1', 'photon2']})
leaves_LL = constr_list('gg_leaves.txt', {'calog' : ['photon'], 'photon_conv' : ['photon_conv']}, ['ele', 'pos'])
leaves_DD = constr_list('gg_leaves.txt', {'calog' : ['photon'], 'photon_conv' : ['photon_conv']}, ['ele', 'pos'])
searchDir = '/Users/sbenson/Documents/STBC-Drive/data/sbenson/BsGammaGamma'

file = 'stripped_BsGGTotal.root'
none_Data = searchDir+'/'+'Data_None_16_MC.h5' 
LL_Data = searchDir+'/'+'Data_LL_16_MC.h5' 
DD_Data = searchDir+'/'+'Data_DD_16_MC.h5' 

print ("Processing file: %s " % file)
f_root = searchDir+'/'+file
t_n = 'Bs2GammaGamma_none/DecayTree'
t_l = 'Bs2GammaGamma_LL/DecayTree'
t_d = 'Bs2GammaGamma_DD/DecayTree'
df_n = read_root(f_root, key=t_n, columns = leaves_none)
df_l = read_root(f_root, key=t_l, columns = leaves_LL)
df_d = read_root(f_root, key=t_d, columns = leaves_DD)
print ("Read successfully")
df_n.to_hdf(none_Data, 'df_n', append=False)
df_l.to_hdf(LL_Data, 'df_l', append=False)
df_d.to_hdf(DD_Data, 'df_d', append=False)
print ("Appended successfully")
