################################################### NECESSARY BLURB #################################################
from Gaudi.Configuration import *
from Configurables import Moore

Moore().ForceSingleL0Configuration = False
Moore().UseTCK = True
Moore().InitialTCK="0x21771801"
Moore().CheckOdin = False
Moore().EnableDataOnDemand = True
Moore().WriterRequires = []
Moore().RemoveInputHltRawBanks=False
Moore().DDDBtag   = "dddb-20150526"
Moore().CondDBtag = "sim-20161124-2-vc-md100"
Moore().DataType = "2016"
Moore().Simulation = True
Moore().OutputLevel = 3
Moore().outputFile = '/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15_hlt2.ldst'

lst = [
"/eos/lhcb/user/s/sbenson/DSTs/GammaGammaNote/ALP15_hlt1.ldst"
]
from GaudiConf import IOHelper
IOHelper().inputFiles([l for l in lst])
#from Configurables import L0Conf
#L0Conf().EnableL0DecodingOnDemand = True
#L0Conf().FastL0DUDecoding = True
#L0Conf().DecodeL0DU = True
#L0Conf().TCK = '0x1801'

