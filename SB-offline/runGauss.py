from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
import os
GaussGen.FirstEventNumber = 1
if 'RUNNUMBER' in os.environ:
    GaussGen.RunNumber        = os.environ['RUNNUMBER']
else:
    GaussGen.RunNumber        = 1010

#--Number of events
if 'EVTMAX' in os.environ:
    nEvts = os.environ['EVTMAX']
else:
    nEvts = 10
LHCbApp().EvtMax = nEvts

print "Runnumber = {} and evtmax = {}".format(GaussGen.RunNumber, LHCbApp().EvtMax)
print "Runnumber = {} and evtmax = {}".format(GaussGen.RunNumber, LHCbApp().EvtMax)
##############################################

#--Pick beam conditions as set in AppConfig
from Gaudi.Configuration import *
#importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py")
importOptions("/project/bfys/sbenson/PG/AsymmetryTools/projects/ParticleGun/options/Beam4000GeV-mu100-JulSep2012-nu2.5.py")
#importOptions('$APPCONFIGOPTS/Gauss/xgen.py')

#############################################

from Gauss.Configuration import *
Gauss().Production = 'PGUN'
idFile = 'Gauss'
OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile
#OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile

#####################################################################
Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'FTFP_BERT', "GeneralPhys":True, "LHCbPhys":True}
from Configurables import Gauss
from Configurables import GenInit
from GaudiKernel import SystemOfUnits
GenInit("GaussGen").BunchSpacing = 25 * SystemOfUnits.ns
from Configurables import Gauss
#Gauss().RICHRandomHits = True

#--Set database tags using those for Sim09b
from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "ZLIB:1"


