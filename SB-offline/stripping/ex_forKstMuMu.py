# EVTMAX!!!!!!!!!!!!
evtmax=-1

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
# Define required DecayTreeTuple ######################################
from Configurables import LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolTrigger, TupleToolTISTOS, TupleToolDecay
from Configurables import TupleToolTagging, DecayTreeTuple, TupleToolMCTruth

#from Configurables import PhysConf
#PhysConf().CaloReProcessing=True
#from Configurables import CondDB,CondDBAccessSvc
#CondDB().addLayer(CondDBAccessSvc('CaloDB',ConnectionString='sqlite_file:/afs/cern.ch/user/o/odescham/ecal7/mySQLDDDB/STRIPPING21/2010-2013-v4/CaloReconstruction-2010-2013-v4.db/LHCBCOND', DefaultTAG='HEAD'))

from Configurables import RawEventJuggler
tck = "0x409f0045" 
MySeq=GaudiSequencer("MoveTCKtoNewLocation")
MyWriter=InputCopyStream("CopyToFile")
RawEventJuggler().TCK=tck
RawEventJuggler().Input=2.0 
RawEventJuggler().Output=0.0 
RawEventJuggler().Sequencer=MySeq
RawEventJuggler().WriterOptItemList=MyWriter
RawEventJuggler().KillInputBanksAfter="L0*|Hlt*"
RawEventJuggler().KillExtraNodes=True 
ApplicationMgr().TopAlg += [MySeq]

# GET THE TRIGGER LIST OUT OF THE WAY
tlist = [

    # L0 decisions
    'L0CALODecision', 'L0PhotonDecision','L0HadronDecision','L0ElectronDecision','L0MuonDecision','L0DiMuonDecision'
    ,'L0ElectronHiDecision','L0ElectronNoSPDDecision'
    ,'L0PhotonHiDecision','L0PhotonNoSPDDecision'
    ,'L0HadronNoSPDDecision' 
    # Hlt1 decisions
    ,'Hlt1DiMuonHighMassDecision'
    ,'Hlt1DiMuonLowMassDecision'
    ,'Hlt1SingleMuonNoIPDecision'
    ,'Hlt1SingleMuonHighPTDecision'
    ,'Hlt1SingleElectronNoIPDecision'
    ,'Hlt1TrackAllL0Decision'
    ,'Hlt1TrackMuonDecision'
    ,'Hlt1TrackPhotonDecision'
    ,'Hlt1TrackForwardPassThroughDecision'
    ,'Hlt1TrackForwardPassThroughLooseDecision'
    ,'Hlt1CharmCalibrationNoBiasDecision'
    ,'Hlt1NoPVPassThroughDecision'
    ,'Hlt1DiProtonDecision'
    ,'Hlt1DiProtonLowMultDecision'
    # All HLT2 physics
    ,'Hlt2SingleTFElectronDecision'
    ,'Hlt2SingleElectronTFLowPtDecision'
    ,'Hlt2SingleElectronTFHighPtDecision'
    ,'Hlt2SingleTFVHighPtElectronDecision'
    ,'Hlt2DiElectronHighMassDecision'
    ,'Hlt2DiElectronBDecision'
    ,'Hlt2B2HHLTUnbiasedDecision'
    ,'Hlt2B2HHLTUnbiasedDetachedDecision'
    ,'Hlt2Topo2BodySimpleDecision'
    ,'Hlt2Topo3BodySimpleDecision'
    ,'Hlt2Topo4BodySimpleDecision'
    ,'Hlt2Topo2BodyBBDTDecision'
    ,'Hlt2Topo3BodyBBDTDecision'
    ,'Hlt2Topo4BodyBBDTDecision'
    ,'Hlt2TopoMu2BodyBBDTDecision'
    ,'Hlt2TopoMu3BodyBBDTDecision'
    ,'Hlt2TopoMu4BodyBBDTDecision'
    ,'Hlt2TopoE2BodyBBDTDecision'
    ,'Hlt2TopoE3BodyBBDTDecision'
    ,'Hlt2TopoE4BodyBBDTDecision'
    ,'Hlt2TopoRad2BodyBBDTDecision'
    ,'Hlt2TopoRad2plus1BodyBBDTDecision'
    ,'Hlt2IncPhiDecision'
    ,'Hlt2IncPhiSidebandsDecision'
    ,'Hlt2CharmHadD02HHKsLLDecision'
    ,'Hlt2Dst2PiD02PiPiDecision'
    ,'Hlt2Dst2PiD02MuMuDecision'
    ,'Hlt2Dst2PiD02KMuDecision'
    ,'Hlt2Dst2PiD02KPiDecision'
    'Hlt2CharmHadD02HH_D02PiPiDecision'
    ,'Hlt2CharmHadD02HH_D02PiPiWideMassDecision'
    ,'Hlt2CharmHadD02HH_D02KKDecision'
    ,'Hlt2CharmHadD02HH_D02KKWideMassDecision'
    ,'Hlt2CharmHadD02HH_D02KPiDecision'
    ,'Hlt2CharmHadD02HH_D02KPiWideMassDecision'
    ,'Hlt2CharmHadLambdaC2KPPiDecision'
    ,'Hlt2Bs2PhiGammaDecision'
    ,'Hlt2Bs2PhiGammaWideBMassDecision'
    ,'Hlt2Bd2KstGammaDecision'
    ,'Hlt2Bd2KstGammaWideKMassDecision'
    ,'Hlt2Bd2KstGammaWideBMassDecision'
    ,'Hlt2CharmHadD2KS0H_D2KS0PiDecision'
    ,'Hlt2CharmHadD2KS0H_D2KS0KDecision'
    ,'Hlt2CharmRareDecayD02MuMuDecision'
    ,'Hlt2B2HHDecision'
    ,'Hlt2MuonFromHLT1Decision'
    ,'Hlt2SingleMuonDecision'
    ,'Hlt2SingleMuonHighPTDecision'
    ,'Hlt2SingleMuonVHighPTDecision'
    ,'Hlt2SingleMuonLowPTDecision'
    ,'Hlt2DiProtonDecision'
    ,'Hlt2DiProtonLowMultDecision'
    ,'Hlt2CharmSemilepD02HMuNu_D02KMuNuWSDecision'
    ,'Hlt2CharmSemilepD02HMuNu_D02PiMuNuWSDecision'
    ,'Hlt2CharmSemilepD02HMuNu_D02KMuNuDecision'
    ,'Hlt2CharmSemilepD02HMuNu_D02KMuNuTightDecision'
    ,'Hlt2CharmSemilepD02HMuNu_D02PiMuNuDecision'
    ,'Hlt2CharmHadMinBiasLambdaC2KPPiDecision'
    ,'Hlt2CharmHadMinBiasD02KPiDecision'
    ,'Hlt2CharmHadMinBiasD02KKDecision'
    ,'Hlt2CharmHadMinBiasDplus2hhhDecision'
    ,'Hlt2CharmHadMinBiasLambdaC2LambdaPiDecision'
    ,'Hlt2TFBc2JpsiMuXDecision'
    ,'Hlt2TFBc2JpsiMuXSignalDecision'
    ,'Hlt2diPhotonDiMuonDecision'
    ,'Hlt2LowMultMuonDecision'
    ,'Hlt2LowMultHadronDecision'
    ,'Hlt2LowMultHadron_nofilterDecision'
    ,'Hlt2LowMultPhotonDecision'
    ,'Hlt2LowMultElectronDecision'
    ,'Hlt2LowMultElectron_nofilterDecision'
    ,'Hlt2DisplVerticesHighMassSingleDecision'
    ,'Hlt2DisplVerticesDoubleDecision'
    ,'Hlt2DisplVerticesHighFDSingleDecision'
    ,'Hlt2DisplVerticesSingleDecision'
    ,'Hlt2DisplVerticesSinglePostScaledDecision'
    ,'Hlt2DisplVerticesDoublePostScaledDecision'
    ,'Hlt2DisplVerticesSingleHighMassPostScaledDecision'
    ,'Hlt2DisplVerticesSingleHighFDPostScaledDecision'
    ,'Hlt2DisplVerticesSingleMVPostScaledDecision'
    ,'Hlt2DisplVerticesSingleDownDecision'
    ,'Hlt2CharmSemilepD2HMuMuDecision'
    ,'Hlt2CharmSemilepD2HMuMuWideMassDecision'
    ,'Hlt2RadiativeTopoTrackTOSDecision'
    ,'Hlt2RadiativeTopoPhotonL0Decision'
    ,'Hlt2B2HHPi0_MergedDecision'
    ,'Hlt2CharmHadD2HHHDecision'
    ,'Hlt2CharmHadD2HHHWideMassDecision'
    ,'Hlt2DiMuonDecision'
    ,'Hlt2DiMuonLowMassDecision'
    ,'Hlt2DiMuonJPsiDecision'
    ,'Hlt2DiMuonJPsiHighPTDecision'
    ,'Hlt2DiMuonPsi2SDecision'
    ,'Hlt2DiMuonPsi2SHighPTDecision'
    ,'Hlt2DiMuonBDecision'
    ,'Hlt2DiMuonZDecision'
    ,'Hlt2DiMuonDY1Decision'
    ,'Hlt2DiMuonDY2Decision'
    ,'Hlt2DiMuonDY3Decision'
    ,'Hlt2DiMuonDY4Decision'
    ,'Hlt2DiMuonDetachedDecision'
    ,'Hlt2DiMuonDetachedHeavyDecision'
    ,'Hlt2DiMuonDetachedJPsiDecision'
    ,'Hlt2TriMuonDetachedDecision'
    ,'Hlt2DoubleDiMuonDecision'
    ,'Hlt2DiMuonAndMuonDecision'
    ,'Hlt2TriMuonTauDecision'
    ,'Hlt2DiMuonAndGammaDecision'
    ,'Hlt2DiMuonAndD0Decision'
    ,'Hlt2DiMuonAndDpDecision'
    ,'Hlt2DiMuonAndDsDecision'
    ,'Hlt2DiMuonAndLcDecision'
    ,'Hlt2CharmSemilepD02HHMuMuHardHadronsSoftMuonsDecision'
    ,'Hlt2CharmSemilepD02HHMuMuHardHadronsSoftMuonsWideMassDecision'
    ,'Hlt2CharmSemilepD02HHMuMuHardHadronsAndMuonsDecision'
    ,'Hlt2CharmSemilepD02HHMuMuHardHadronsAndMuonsWideMassDecision'
    ,'Hlt2CharmSemilepD02HHMuMuDecision'
    ,'Hlt2CharmSemilepD02HHMuMuWideMassDecision'
    ,'Hlt2CharmHadD02HHHHDecision'
    ,'Hlt2CharmHadD02HHHHWideMassDecision'
]
toollist=[
    	 "TupleToolPrimaries" 
    	, "TupleToolTrackInfo"                
    	, "TupleToolPhotonInfo" 
    	, "TupleToolTISTOS"
    	, "TupleToolMCTruth"
    	, "TupleToolMCBackgroundInfo"
    	#
    	, "TupleToolKinematic" 
    	, "TupleToolPid"       
    	, "TupleToolEventInfo" 
    	, "TupleToolRecoStats"  
   	#
    	#, "TupleToolVtxIsoln"
    	, "TupleToolL0Data"
    	, "TupleToolCaloHypo"
]
def makeTruth(input):
	MCTruth = TupleToolMCTruth() 
	MCTruth.ToolList = [
	    "MCTupleToolKinematic",   
	    "MCTupleToolHierarchy",
	    "MCTupleToolPID"
	]
	input.addTool(MCTruth )

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ]} 

# Now build the stream
from StrippingConf.StrippingStream import StrippingStream
streamBhadronCompleteEvent = StrippingStream("StreamBhadronCompleteEvent")
bhadroncompleteeventLines = []

#
# Import your stripping lines
#
from StrippingSelections.StrippingBs2gammagamma import StrippingBs2gammagammaConf
from StrippingSelections.StrippingBs2gammagamma import default_config as config_Bs2gg
confBs2gg = StrippingBs2gammagammaConf("Bs2gammagamma",config_Bs2gg['CONFIG'])
for l in confBs2gg.lines():
    bhadroncompleteeventLines.append(l)

streamBhadronCompleteEvent.appendLines(bhadroncompleteeventLines)

from Configurables import  ProcStatusCheck
filterBadEvents =  ProcStatusCheck()

sc = StrippingConf( Streams = [ streamBhadronCompleteEvent ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    TESPrefix = 'Strip'
                    )
#print str(streamBhadronCompleteEvent.outputLocations()[0])


### OUTPUT TO DST THE TRUTH MATCHED STRIPPED CANDIDATES ###############
from Configurables import FilterDesktop
# LL1
_Filter_LL1 = FilterDesktop(name="FilterTruth_1ConvLL",Code = "mcMatch( 'B_s0 -> gamma gamma' )")
_Filter_LL1.Preambulo=["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
LL1_Cands = DataOnDemand(Location=streamBhadronCompleteEvent.outputLocations()[0])
Bs2GG_LL1 = Selection("SelBs2GG_LL1", 
                    Algorithm = _Filter_LL1, 
                    RequiredSelections = [ LL1_Cands ])
SeqBs2GG_LL1 = SelectionSequence('SeqBs2GG_LL1', TopSelection = Bs2GG_LL1)
# DD1
_Filter_DD1 = FilterDesktop(name="FilterTruth_1ConvDD",Code = "mcMatch( 'B_s0 -> gamma gamma' )")
_Filter_DD1.Preambulo=["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
DD1_Cands = DataOnDemand(Location=streamBhadronCompleteEvent.outputLocations()[1])
Bs2GG_DD1 = Selection("SelBs2GG_DD1", 
                    Algorithm = _Filter_DD1, 
                    RequiredSelections = [ DD1_Cands ])
SeqBs2GG_DD1 = SelectionSequence('SeqBs2GG_DD1', TopSelection = Bs2GG_DD1)
# 2 Conv
_Filter_2 = FilterDesktop(name="FilterTruth_2Conv",Code = "mcMatch( 'B_s0 -> gamma gamma' )")
_Filter_2.Preambulo=["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
Cands2 = DataOnDemand(Location=streamBhadronCompleteEvent.outputLocations()[2])
Bs2GG_2 = Selection("SelBs2GG_2", 
                    Algorithm = _Filter_2, 
                    RequiredSelections = [ Cands2 ])
SeqBs2GG_2 = SelectionSequence('SeqBs2GG_2', TopSelection = Bs2GG_2)
# None
_Filter_none = FilterDesktop(name="FilterTruth_none",Code = "mcMatch( 'B_s0 -> gamma gamma' )")
_Filter_none.Preambulo=["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
Cands_none = DataOnDemand(Location=streamBhadronCompleteEvent.outputLocations()[3])
Bs2GG_none = Selection("SelBs2GG_none", 
                    Algorithm = _Filter_none, 
                    RequiredSelections = [ Cands_none ])
SeqBs2GG_none = SelectionSequence('SeqBs2GG_none', TopSelection = Bs2GG_none)

### WRITE THE DST ###
from Configurables import InputCopyStream
from DSTWriters.streamconf import OutputStreamConf
from DSTWriters.Configuration import SelDSTWriter
streamConf = OutputStreamConf(streamType = InputCopyStream,
                                      fileExtension = '.dst')
SelDSTWriterElements = {'default' : []}
SelDSTWriterConf = {'default' : streamConf}

dstWriter_LL1 = SelDSTWriter('FullDST_LL1',
                                 StreamConf         = SelDSTWriterConf,
                                 MicroDSTElements   = SelDSTWriterElements,
                                 WriteFSR           = True,
                                 OutputFileSuffix   = 'Truth',
                                 SelectionSequences = [SeqBs2GG_LL1])
dstWriter_DD1 = SelDSTWriter('FullDST_DD1',
                                 StreamConf         = SelDSTWriterConf,
                                 MicroDSTElements   = SelDSTWriterElements,
                                 WriteFSR           = True,
                                 OutputFileSuffix   = 'Truth',
                                 SelectionSequences = [SeqBs2GG_DD1])
dstWriter_2 = SelDSTWriter('FullDST_2',
                                 StreamConf         = SelDSTWriterConf,
                                 MicroDSTElements   = SelDSTWriterElements,
                                 WriteFSR           = True,
                                 OutputFileSuffix   = 'Truth',
                                 SelectionSequences = [SeqBs2GG_2])
dstWriter_none = SelDSTWriter('FullDST_none',
                                 StreamConf         = SelDSTWriterConf,
                                 MicroDSTElements   = SelDSTWriterElements,
                                 WriteFSR           = True,
                                 OutputFileSuffix   = 'Truth',
                                 SelectionSequences = [SeqBs2GG_none])

# Make the DecayTreeTuples
# LL *******
tuple_LL = DecayTreeTuple("Bs2GammaGamma_LL")
tuple_LL.Inputs = [ SeqBs2GG_LL1.outputLocation() ]
tuple_LL.ToolList+=toollist
#
tuple_LL.Decay = "B_s0 -> ^(gamma -> ^e+ ^e-) ^gamma"
tuple_LL.addBranches({
	"B_s0"    : "B_s0"
    	,"photon_conv"   : "B_s0 -> ^(gamma -> e+ e-) gamma"
    	,"ele"   : "B_s0 -> (gamma -> e+ ^e-) gamma"
    	,"pos"   : "B_s0 -> (gamma -> ^e+ e-) gamma"
    	,"photon": "B_s0 -> (gamma -> e+ e-) ^gamma"
})
#
tuple_LL.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_LL.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_LL.TupleToolTrigger.Verbose = True
tuple_LL.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_LL.TupleToolTISTOS.Verbose = True
tuple_LL.TupleToolTISTOS.TriggerList = tlist
#
tuple_LL.TupleToolTISTOS.VerboseHlt1 = True
tuple_LL.TupleToolTISTOS.VerboseHlt2 = True
tuple_LL.TupleToolTISTOS.VerboseL0 = True
makeTruth(tuple_LL)
# DD *******
tuple_DD = DecayTreeTuple("Bs2GammaGamma_DD")
tuple_DD.Inputs = [ SeqBs2GG_DD1.outputLocation() ]
tuple_DD.ToolList+=toollist
#
tuple_DD.Decay = "B_s0 -> ^(gamma -> ^e+ ^e-) ^gamma"
tuple_DD.addBranches({
	"B_s0"    : "B_s0"
    	,"photon_conv"   : "B_s0 -> ^(gamma -> e+ e-) gamma"
    	,"ele"   : "B_s0 -> (gamma -> e+ ^e-) gamma"
    	,"pos"   : "B_s0 -> (gamma -> ^e+ e-) gamma"
    	,"photon": "B_s0 -> (gamma -> e+ e-) ^gamma"
})
#
tuple_DD.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_DD.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_DD.TupleToolTrigger.Verbose = True
tuple_DD.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_DD.TupleToolTISTOS.Verbose = True
tuple_DD.TupleToolTISTOS.TriggerList = tlist
#
tuple_DD.TupleToolTISTOS.VerboseHlt1 = True
tuple_DD.TupleToolTISTOS.VerboseHlt2 = True
tuple_DD.TupleToolTISTOS.VerboseL0 = True
makeTruth(tuple_DD)
# double *******
tuple_double = DecayTreeTuple("Bs2GammaGamma_double")
tuple_double.Inputs = [ SeqBs2GG_2.outputLocation() ]
tuple_double.ToolList+=toollist
#
tuple_double.Decay = "B_s0 -> ^(gamma -> ^e+ ^e-) ^(gamma -> ^e+ ^e-)"
tuple_double.addBranches({
	"B_s0"    : "B_s0"
    	,"photon_conv1"   : "B_s0 -> ^(gamma -> e+ e-) (gamma -> e+ e-)"
    	,"ele"   : "B_s0 -> (gamma -> e+ ^e-) (gamma -> e+ e-)"
    	,"pos"   : "B_s0 -> (gamma -> ^e+ e-) (gamma -> e+ e-)"
    	,"photon_conv2"   : "B_s0 -> (gamma -> e+ e-) ^(gamma -> e+ e-)"
    	,"ele2"   : "B_s0 -> (gamma -> e+ e-) (gamma -> e+ ^e-)"
    	,"pos2"   : "B_s0 -> (gamma -> e+ e-) (gamma -> ^e+ e-)"
})
#
tuple_double.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_double.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_double.TupleToolTrigger.Verbose = True
tuple_double.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_double.TupleToolTISTOS.Verbose = True
tuple_double.TupleToolTISTOS.TriggerList = tlist
#
tuple_double.TupleToolTISTOS.VerboseHlt1 = True
tuple_double.TupleToolTISTOS.VerboseHlt2 = True
tuple_double.TupleToolTISTOS.VerboseL0 = True
makeTruth(tuple_double)
# none *******
tuple_none = DecayTreeTuple("Bs2GammaGamma_none")
tuple_none.Inputs = [ SeqBs2GG_none.outputLocation() ]
tuple_none.ToolList+=toollist
#
tuple_none.Decay = "B_s0 -> ^gamma ^gamma"
tuple_none.addBranches({
	"B_s0"    : "B_s0"
    	,"photon1"   : "B_s0 -> ^gamma gamma"
        ,"photon2"   : "B_s0 -> gamma ^gamma"
})
#
tuple_none.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_none.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_none.TupleToolTrigger.Verbose = True
tuple_none.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_none.TupleToolTISTOS.Verbose = True
tuple_none.TupleToolTISTOS.TriggerList = tlist
#
tuple_none.TupleToolTISTOS.VerboseHlt1 = True
tuple_none.TupleToolTISTOS.VerboseHlt2 = True
tuple_none.TupleToolTISTOS.VerboseL0 = True
makeTruth(tuple_none)

DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().Simulation   = True
DaVinci().EvtMax = evtmax
DaVinci().PrintFreq = 2000
DaVinci().UserAlgorithms+=[ sc.sequence() ]
DaVinci().UserAlgorithms+=[ SeqBs2GG_LL1.sequence(), dstWriter_LL1.sequence() ]
DaVinci().UserAlgorithms+=[ SeqBs2GG_DD1.sequence(), dstWriter_DD1.sequence() ]
DaVinci().UserAlgorithms+=[ SeqBs2GG_2.sequence(), dstWriter_2.sequence() ]
DaVinci().UserAlgorithms+=[ SeqBs2GG_none.sequence(), dstWriter_none.sequence() ]
DaVinci().UserAlgorithms+=[ tuple_LL,tuple_DD,tuple_double,tuple_none ]
DaVinci().DataType  = "2012"
DaVinci().InputType = "DST"
DaVinci().RootCompressionLevel = "ZLIB:1"
DaVinci().TupleFile = 'ReduceGammaGammaTest.root'

# change the column size of timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

# database
DaVinci().DDDBtag  = "dddb-20130929-1"
DaVinci().CondDBtag = "sim-20130522-1-vc-md100"

#from GaudiConf import IOHelper
#IOHelper().inputFiles( [ "/tmp/sbenson/00035842_00000008_1.allstreams.dst" ] ) 
