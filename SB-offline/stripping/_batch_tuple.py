from Gaudi.Configuration import * 
from Configurables import DaVinci, TupleToolTagging, DecayTreeTuple, TupleToolMCTruth
from Configurables import LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolTrigger, TupleToolTISTOS, TupleToolDecay
from Configurables import DaVinci,HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder, BackgroundCategory, TupleToolRecoStats, TupleToolP2VV
from Configurables import FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonParticles.Utils import *
from GaudiKernel.SystemOfUnits import *
from Configurables import ( DiElectronMaker, ProtoParticleCALOFilter,
		                            OfflineVertexFitter, ParticleTransporter, BremAdder )

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ]} 
#                                "GhostProbCut" : [0, 0.35 ] }

# Now build the stream
from StrippingConf.StrippingStream import StrippingStream
streamBhadronCompleteEvent = StrippingStream("StreamBhadronCompleteEvent")
bhadroncompleteeventLines = []

#
# Import your stripping lines
#
from StrippingArchive.Stripping28.StrippingRD.StrippingBs2gammagamma import StrippingBs2gammagammaConf
from StrippingArchive.Stripping28.StrippingRD.StrippingBs2gammagamma import default_config as config_Bs2gg
confBs2gg = StrippingBs2gammagammaConf("Bs2gammagamma",config_Bs2gg["CONFIG"])
for l in confBs2gg.lines():
    bhadroncompleteeventLines.append(l)

streamBhadronCompleteEvent.appendLines(bhadroncompleteeventLines)

from Configurables import  ProcStatusCheck
filterBadEvents =  ProcStatusCheck()

from StrippingConf.Configuration import StrippingConf
sc = StrippingConf( Streams = [ streamBhadronCompleteEvent ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    TESPrefix = 'Strip'
                    )

# GET THE TRIGGER LIST OUT OF THE WAY
tlist = [
    # L0 decisions
    'L0CALODecision', 'L0PhotonDecision','L0HadronDecision','L0ElectronDecision','L0MuonDecision','L0DiMuonDecision'
    # Hlt1 decisions
    ,'Hlt1DiMuonHighMassDecision'
    ,'Hlt1DiMuonLowMassDecision'
    ,'Hlt1SingleMuonNoIPDecision'
    ,'Hlt1SingleMuonHighPTDecision'
    ,'Hlt1SingleElectronNoIPDecision'
    ,'Hlt1TrackMVADecision'
    ,'Hlt1TwoTrackMVADecision'
    ,'Hlt1TrackMuonDecision'
    ,'Hlt1TrackPhotonDecision'
    ,'Hlt1TrackForwardPassThroughDecision'
    ,'Hlt1TrackForwardPassThroughLooseDecision'
    ,'Hlt1CharmCalibrationNoBiasDecision'
    ,'Hlt1NoPVPassThroughDecision'
    ,'Hlt1DiProtonDecision'
    ,'Hlt1DiProtonLowMultDecision'
    # All HLT2 physics
    ,'Hlt2Topo2BodyBBDTDecision'
    ,'Hlt2Topo3BodyBBDTDecision'
    ,'Hlt2Topo4BodyBBDTDecision'
    ,'Hlt2TopoMu2BodyBBDTDecision'
    ,'Hlt2TopoMu3BodyBBDTDecision'
    ,'Hlt2TopoMu4BodyBBDTDecision'
    ,'Hlt2TopoE2BodyBBDTDecision'
    ,'Hlt2TopoE3BodyBBDTDecision'
    ,'Hlt2TopoE4BodyBBDTDecision'
    ,'Hlt2TopoRad2BodyBBDTDecision'
    ,'Hlt2TopoRad2plus1BodyBBDTDecision'
    ,'Hlt2IncPhiDecision'
    ,'Hlt2Bs2PhiGammaDecision'
    ,'Hlt2Bs2PhiGammaWideBMassDecision'
    ,'Hlt2Bd2KstGammaDecision'
    ,'Hlt2Bd2KstGammaWideKMassDecision'
    ,'Hlt2Bd2KstGammaWideBMassDecision'
    ,'Hlt2RadiativeTopoTrackTOSDecision'
    ,'Hlt2RadiativeTopoPhotonL0Decision'
    ,'Hlt2RadiativeB2GammaGammaDecision'
    ,'Hlt2RadiativeB2GammaGammaLLDecision'
    ,'Hlt2RadiativeB2GammaGammaDDDecision'
    ,'Hlt2RadiativeB2GammaGammaDoubleDecision'
]
toollist=[
    	 "TupleToolPrimaries" 
    	, "TupleToolTrackInfo"                
    	, "TupleToolPhotonInfo" 
    	, "TupleToolTISTOS"
    	#
    	, "TupleToolKinematic" 
    	, "TupleToolPid"       
    	, "TupleToolEventInfo" 
    	, "TupleToolRecoStats"  
   	#
    	, "TupleToolL0Data"
    	, "TupleToolCaloHypo"
]
###### ADDITIONAL LOKI VARIABLES
myLoKi_Basic=LoKi__Hybrid__TupleTool("myLoKi_Basic")
myLoKi_Basic.Preambulo+=["from LoKiProtoParticles.decorators import *","from LoKiTracks.decorators import *"]
myLoKi_Basic.Variables = {
    # "name"  : " functor" ,
"LOKI_IsPhoton":  "PPINFO(LHCb.ProtoParticle.IsPhoton,-1000)"
,"LOKI_IsNotE" :  "PPINFO(LHCb.ProtoParticle.IsNotE,-1000)"
,"LOKI_IsNotH" :  "PPINFO(LHCb.ProtoParticle.IsNotH,-1000)"
,"LOKI_EcalPIDe" :  "PPINFO(LHCb.ProtoParticle.EcalPIDe,-1000)"
,"LOKI_PrsPIDe" :  "PPINFO(LHCb.ProtoParticle.PrsPIDe,-1000)"
,"LOKI_BremPIDe" :  "PPINFO(LHCb.ProtoParticle.BremPIDe,-1000)"
,"LOKI_HcalPIDe" :  "PPINFO(LHCb.ProtoParticle.HcalPIDe,-1000)"
,"LOKI_HcalPIDmu" :  "PPINFO(LHCb.ProtoParticle.HcalPIDmu,-1000)"
,"LOKI_EcalPIDmu" :  "PPINFO(LHCb.ProtoParticle.EcalPIDmu,-1000)"
,"LOKI_CaloTrMatch" :  "PPINFO(LHCb.ProtoParticle.CaloTrMatch,-1000)"
,"LOKI_CaloTrMatch" : "PPINFO(LHCb.ProtoParticle.CaloElectronMatch,-1000)"
,"LOKI_CaloBremMatch" : "PPINFO(LHCb.ProtoParticle.CaloBremMatch,-1000)"
,"LOKI_CaloNeutralSpd" : "PPINFO(LHCb.ProtoParticle.CaloNeutralSpd,-1000)"
,"LOKI_CaloNeutralPrs" : "PPINFO(LHCb.ProtoParticle.CaloNeutralPrs,-1000)"
,"LOKI_CaloNeutralEcal" : "PPINFO(LHCb.ProtoParticle.CaloNeutralEcal,-1000)"
,"LOKI_CaloNeutralHcal2Ecal" : "PPINFO(LHCb.ProtoParticle.CaloNeutralHcal2Ecal,-1000)"
,"LOKI_CaloNeutralE49" : "PPINFO(LHCb.ProtoParticle.CaloNeutralE49,-1000)"
,"LOKI_CaloNeutralID" : "PPINFO(LHCb.ProtoParticle.CaloNeutralID,-1000)"
,"LOKI_CaloDepositID" : "PPINFO(LHCb.ProtoParticle.CaloDepositID,-1000)"
,"LOKI_ShowerShape" : "PPINFO(LHCb.ProtoParticle.ShowerShape,-1000)"
,"LOKI_ClusterMass" : "PPINFO(LHCb.ProtoParticle.ClusterMass,-1000)"
,"LOKI_CaloSpdE" : "PPINFO(LHCb.ProtoParticle.CaloSpdE,-1000)"
,"LOKI_CaloPrsE" : "PPINFO(LHCb.ProtoParticle.CaloPrsE,-1000)"
,"LOKI_CaloEcalE" : "PPINFO(LHCb.ProtoParticle.CaloEcalE,-1000)"
,"LOKI_CaloHcalE" : "PPINFO(LHCb.ProtoParticle.CaloHcalE,-1000)"
,"LOKI_CaloEcalChi2" : "PPINFO(LHCb.ProtoParticle.CaloEcalChi2,-1000)"
,"LOKI_CaloBremChi2" : "PPINFO(LHCb.ProtoParticle.CaloBremChi2,-1000)"
,"LOKI_CaloClusChi2" : "PPINFO(LHCb.ProtoParticle.CaloClusChi2,-1000)"
,"LOKI_CaloNeutralPrsM" : "PPINFO(LHCb.ProtoParticle.CaloNeutralPrsM,-1000)"
,"LOKI_CaloShapeFr2r4" : "PPINFO(LHCb.ProtoParticle.CaloShapeFr2r4,-1000)"
,"LOKI_CaloShapeKappa" : "PPINFO(LHCb.ProtoParticle.CaloShapeKappa,-1000)"
,"LOKI_CaloShapeAsym" : "PPINFO(LHCb.ProtoParticle.CaloShapeAsym,-1000)"
,"LOKI_CaloShapeE1" : "PPINFO(LHCb.ProtoParticle.CaloShapeE1,-1000)"
,"LOKI_CaloShapeE2" : "PPINFO(LHCb.ProtoParticle.CaloShapeE2,-1000)"
,"LOKI_CaloPrsShapeE2" : "PPINFO(LHCb.ProtoParticle.CaloPrsShapeE2,-1000)"
,"LOKI_CaloPrsShapeEmax" : "PPINFO(LHCb.ProtoParticle.CaloPrsShapeEmax,-1000)"
,"LOKI_CaloPrsShapeFr2" : "PPINFO(LHCb.ProtoParticle.CaloPrsShapeFr2,-1000)"
,"LOKI_CaloPrsShapeAsym" : "PPINFO(LHCb.ProtoParticle.CaloPrsShapeAsym,-1000)"
,"LOKI_CaloPrsM" : "PPINFO(LHCb.ProtoParticle.CaloPrsM,-1000)"
,"LOKI_CaloPrsM15" : "PPINFO(LHCb.ProtoParticle.CaloPrsM15,-1000)"
,"LOKI_CaloPrsM30" : "PPINFO(LHCb.ProtoParticle.CaloPrsM30,-1000)"
,"LOKI_CaloPrsM45" : "PPINFO(LHCb.ProtoParticle.CaloPrsM45,-1000)"
,"LOKI_CaloClusterCode" : "PPINFO(LHCb.ProtoParticle.CaloClusterCode,-1000)"
,"LOKI_CaloClusterFrac" : "PPINFO(LHCb.ProtoParticle.CaloClusterFrac,-1000)"
,"LOKI_CombDLLe" : "PPINFO(LHCb.ProtoParticle.CombDLLe,-1000)"
,"LOKI_CombDLLmu" : "PPINFO(LHCb.ProtoParticle.CombDLLmu,-1000)"
,"LOKI_CombDLLpi" : "PPINFO(LHCb.ProtoParticle.CombDLLpi,-1000)"
,"LOKI_CombDLLk" : "PPINFO(LHCb.ProtoParticle.CombDLLk,-1000)"
,"LOKI_CombDLLp" : "PPINFO(LHCb.ProtoParticle.CombDLLp,-1000)"
,"LOKI_InAccBrem" : "PPINFO(LHCb.ProtoParticle.InAccBrem,-1000)"
,"LOKI_InAccSpd" : "PPINFO(LHCb.ProtoParticle.InAccSpd,-1000)"
,"LOKI_InAccPrs" : "PPINFO(LHCb.ProtoParticle.InAccPrs,-1000)"
,"LOKI_InAccEcal" : "PPINFO(LHCb.ProtoParticle.InAccEcal,-1000)"
,"LOKI_InAccHcal" : "PPINFO(LHCb.ProtoParticle.InAccHcal,-1000)"
}
#
myLoKi_NonBasic=LoKi__Hybrid__TupleTool("myLoKi_NonBasic")
myLoKi_NonBasic.Variables = {
    # "name"  : " functor" ,
"LOKI_BPVIPCHI2":  "BPVIPCHI2()"
,"LOKI_BPVIP":  "BPVIP()"
,"LOKI_DIRA":  "BPVDIRA"
,"LOKI_FD":  "BPVVDR"
,"LOKI_ENDVERTEX_CHI2DOF":  "VFASPF(VCHI2/VDOF)"
}

# Make the DecayTreeTuples
# LL *******
tuple_LL = DecayTreeTuple("Bs2GammaGamma_LL")
tuple_LL.Inputs = [ streamBhadronCompleteEvent.outputLocations()[0] ]
tuple_LL.ToolList=toollist
#
tuple_LL.Decay = "B_s0 -> ^(gamma -> ^e+ ^e-) ^gamma"
tuple_LL.addBranches({
	"B_s0"    : "B_s0"
    	,"photon_conv"   : "B_s0 -> ^(gamma -> e+ e-) gamma"
    	,"ele"   : "B_s0 -> (gamma -> e+ ^e-) gamma"
    	,"pos"   : "B_s0 -> (gamma -> ^e+ e-) gamma"
    	,"photon": "B_s0 -> (gamma -> e+ e-) ^gamma"
})
#
tuple_LL.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_LL.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_LL.TupleToolTrigger.Verbose = True
tuple_LL.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_LL.TupleToolTISTOS.Verbose = True
tuple_LL.TupleToolTISTOS.TriggerList = tlist
#
tuple_LL.TupleToolTISTOS.VerboseHlt1 = True
tuple_LL.TupleToolTISTOS.VerboseHlt2 = True
tuple_LL.TupleToolTISTOS.VerboseL0 = True
#
tuple_LL.addTool(TupleToolDecay, name="B_s0")
tuple_LL.addTool(TupleToolDecay, name="photon_conv")
tuple_LL.addTool(TupleToolDecay, name="ele")
tuple_LL.addTool(TupleToolDecay, name="pos")
tuple_LL.addTool(TupleToolDecay, name="photon")

# DD *******
tuple_DD = DecayTreeTuple("Bs2GammaGamma_DD")
tuple_DD.Inputs = [ "/Event/AllStreams/Phys/Bs2GammaGamma_DDLine/Particles" ]
tuple_DD.Inputs = [ streamBhadronCompleteEvent.outputLocations()[1] ]
tuple_DD.ToolList=toollist
#
tuple_DD.Decay = "B_s0 -> ^(gamma -> ^e+ ^e-) ^gamma"
tuple_DD.addBranches({
	"B_s0"    : "B_s0"
    	,"photon_conv"   : "B_s0 -> ^(gamma -> e+ e-) gamma"
    	,"ele"   : "B_s0 -> (gamma -> e+ ^e-) gamma"
    	,"pos"   : "B_s0 -> (gamma -> ^e+ e-) gamma"
    	,"photon": "B_s0 -> (gamma -> e+ e-) ^gamma"
})
tuple_DD.addTool(TupleToolDecay, name="B_s0")
tuple_DD.addTool(TupleToolDecay, name="photon_conv")
tuple_DD.addTool(TupleToolDecay, name="ele")
tuple_DD.addTool(TupleToolDecay, name="pos")
tuple_DD.addTool(TupleToolDecay, name="photon")
#
tuple_DD.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_DD.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_DD.TupleToolTrigger.Verbose = True
tuple_DD.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_DD.TupleToolTISTOS.Verbose = True
tuple_DD.TupleToolTISTOS.TriggerList = tlist
#
tuple_DD.TupleToolTISTOS.VerboseHlt1 = True
tuple_DD.TupleToolTISTOS.VerboseHlt2 = True
tuple_DD.TupleToolTISTOS.VerboseL0 = True
# double *******
tuple_double = DecayTreeTuple("Bs2GammaGamma_double")
tuple_double.Inputs = [ "/Event/AllStreams/Phys/Bs2GammaGamma_doubleLine/Particles" ]
tuple_double.Inputs = [ streamBhadronCompleteEvent.outputLocations()[2] ]
tuple_double.ToolList=toollist
#
tuple_double.Decay = "B_s0 -> ^(gamma -> ^e+ ^e-) ^(gamma -> ^e+ ^e-)"
tuple_double.addBranches({
	"B_s0"    : "B_s0"
    	,"photon_conv1"   : "B_s0 -> ^(gamma -> e+ e-) (gamma -> e+ e-)"
    	,"ele"   : "B_s0 -> (gamma -> e+ ^e-) (gamma -> e+ e-)"
    	,"pos"   : "B_s0 -> (gamma -> ^e+ e-) (gamma -> e+ e-)"
    	,"photon_conv2"   : "B_s0 -> (gamma -> e+ e-) ^(gamma -> e+ e-)"
    	,"ele2"   : "B_s0 -> (gamma -> e+ e-) (gamma -> e+ ^e-)"
    	,"pos2"   : "B_s0 -> (gamma -> e+ e-) (gamma -> ^e+ e-)"
})
#
tuple_double.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_double.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_double.TupleToolTrigger.Verbose = True
tuple_double.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_double.TupleToolTISTOS.Verbose = True
tuple_double.TupleToolTISTOS.TriggerList = tlist
#
tuple_double.TupleToolTISTOS.VerboseHlt1 = True
tuple_double.TupleToolTISTOS.VerboseHlt2 = True
tuple_double.TupleToolTISTOS.VerboseL0 = True
#
tuple_double.addTool(TupleToolDecay, name="B_s0")
tuple_double.addTool(TupleToolDecay, name="photon_conv1")
tuple_double.addTool(TupleToolDecay, name="ele")
tuple_double.addTool(TupleToolDecay, name="pos")
tuple_double.addTool(TupleToolDecay, name="photon_conv2")
tuple_double.addTool(TupleToolDecay, name="ele2")
tuple_double.addTool(TupleToolDecay, name="pos2")


# none *******
tuple_none = DecayTreeTuple("Bs2GammaGamma_none")
tuple_none.Inputs = [ "/Event/AllStreams/Phys/Bs2GammaGamma_NoConvLine/Particles" ]
tuple_none.Inputs = [ streamBhadronCompleteEvent.outputLocations()[3] ]
tuple_none.ToolList=toollist
#
tuple_none.Decay = "B_s0 -> ^gamma ^gamma"
tuple_none.addBranches({
	"B_s0"    : "B_s0"
    	,"photon1"   : "B_s0 -> ^gamma gamma"
    	,"photon2"   : "B_s0 -> gamma ^gamma"
})
#
tuple_none.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_none.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_none.TupleToolTrigger.Verbose = True
tuple_none.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_none.TupleToolTISTOS.Verbose = True
tuple_none.TupleToolTISTOS.TriggerList = tlist
#
tuple_none.TupleToolTISTOS.VerboseHlt1 = True
tuple_none.TupleToolTISTOS.VerboseHlt2 = True
tuple_none.TupleToolTISTOS.VerboseL0 = True
#
tuple_none.addTool(TupleToolDecay, name="B_s0")
tuple_none.addTool(TupleToolDecay, name="photon1")
tuple_none.addTool(TupleToolDecay, name="photon2")

# none wide *******
tuple_noneW = DecayTreeTuple("Bs2GammaGamma_noneWide")
tuple_noneW.Inputs = [ "/Event/AllStreams/Phys/Bs2GammaGammaWide_NoConvLine/Particles" ]
tuple_noneW.Inputs = [ streamBhadronCompleteEvent.outputLocations()[4] ]
tuple_noneW.ToolList=toollist
#
tuple_noneW.Decay = "B_s0 -> ^gamma ^gamma"
tuple_noneW.addBranches({
	"B_s0"    : "B_s0"
    	,"photon1"   : "B_s0 -> ^gamma gamma"
    	,"photon2"   : "B_s0 -> gamma ^gamma"
})
#
tuple_noneW.addTool(TupleToolTrigger, name="TupleToolTrigger")
tuple_noneW.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
# Get trigger info
tuple_noneW.TupleToolTrigger.Verbose = True
tuple_noneW.TupleToolTrigger.TriggerList = tlist
# Get TISTOS info
tuple_noneW.TupleToolTISTOS.Verbose = True
tuple_noneW.TupleToolTISTOS.TriggerList = tlist
#
tuple_noneW.TupleToolTISTOS.VerboseHlt1 = True
tuple_noneW.TupleToolTISTOS.VerboseHlt2 = True
tuple_noneW.TupleToolTISTOS.VerboseL0 = True
#
tuple_noneW.addTool(TupleToolDecay, name="B_s0")
tuple_noneW.addTool(TupleToolDecay, name="photon1")
tuple_noneW.addTool(TupleToolDecay, name="photon2")

###### ADDITIONAL LOKI VARIABLES
tuple_LL.photon_conv.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_NonBasic"]
tuple_LL.photon_conv.addTool(myLoKi_NonBasic)
tuple_LL.ele.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_LL.ele.addTool(myLoKi_Basic)
tuple_LL.pos.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_LL.pos.addTool(myLoKi_Basic)
tuple_LL.photon.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_LL.photon.addTool(myLoKi_Basic)
#
tuple_DD.photon_conv.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_NonBasic"]
tuple_DD.photon_conv.addTool(myLoKi_NonBasic)
tuple_DD.ele.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_DD.ele.addTool(myLoKi_Basic)
tuple_DD.pos.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_DD.pos.addTool(myLoKi_Basic)
tuple_DD.photon.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_DD.photon.addTool(myLoKi_Basic)
#
tuple_double.B_s0.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_NonBasic"]
tuple_double.B_s0.addTool(myLoKi_NonBasic)
tuple_double.photon_conv1.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_NonBasic"]
tuple_double.photon_conv1.addTool(myLoKi_NonBasic)
tuple_double.ele.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_double.ele.addTool(myLoKi_Basic)
tuple_double.pos.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_double.pos.addTool(myLoKi_Basic)
tuple_double.photon_conv2.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_NonBasic"]
tuple_double.photon_conv2.addTool(myLoKi_NonBasic)
tuple_double.ele2.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_double.ele2.addTool(myLoKi_Basic)
tuple_double.pos2.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_double.pos2.addTool(myLoKi_Basic)
#
tuple_none.photon1.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_none.photon1.addTool(myLoKi_Basic)
tuple_none.photon2.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_none.photon2.addTool(myLoKi_Basic)
#
tuple_noneW.photon1.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_noneW.photon1.addTool(myLoKi_Basic)
tuple_noneW.photon2.ToolList+=["LoKi::Hybrid::TupleTool/myLoKi_Basic"]
tuple_noneW.photon2.addTool(myLoKi_Basic)

DaVinci().UserAlgorithms += [sc.sequence(),tuple_LL,tuple_DD,tuple_double,tuple_none,tuple_noneW] 

